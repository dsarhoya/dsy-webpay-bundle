<?php

namespace AppBundle\Subscribers;

use dsarhoya\CajaBundle\Entity\OneClickInscription;
use dsarhoya\WebpayBundle\Event\DSYWebpayBundleEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\GetResponseForControllerResultEvent;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Templating\EngineInterface;

class OneClickInscriptionSubscriber implements EventSubscriberInterface
{
    /** @var RouterInterface */
    private $router;

    /** @var EngineInterface */
    private $templating;

    public function __construct(RouterInterface $router, EngineInterface $templating)
    {
        $this->router = $router;
        $this->templating = $templating;
    }

    public function onDsywebpaybundlePostoneclickinscription(GetResponseForControllerResultEvent $event)
    {
        /** @var OneClickInscription $oneClickInscription */
        $oneClickInscription = $event->getControllerResult();

        $route = $this->router->generate('inscription', [
            'cameFrom' => 'oneclick',
            'status' => null === $oneClickInscription->getOneClickToken() ? 'error' : 'success',
            'id' => $oneClickInscription->getId(),
        ]);

        $newRespose = new RedirectResponse($route);

        $event->setResponse($newRespose);
    }

    public function onDsywebpaybundleNormalpaymentsuccess(GetResponseForControllerResultEvent $event)
    {
        $order = $event->getControllerResult();

        $newRespose = new Response();
        $newRespose->setContent($this->templating->render(
            ':normal:success.html.twig', [
                'order' => $order,
            ]
        ));
        $event->setResponse($newRespose);
    }

    public function onDsywebpaybundleNormalpaymenterror(GetResponseForControllerResultEvent $event)
    {
        /* @var OneClickInscription $oneClickInscription */
        /*$oneClickInscription = $event->getControllerResult();
        $newRespose->setContent($this->templating->render(
            ':normal:error.html.twig', [
                'order' => $order,
            ]
        ));
        $event->setResponse($newRespose);*/
    }

    /**
     * Returns an array of event names this subscriber wants to listen to.
     *
     * The array keys are event names and the value can be:
     *
     *  * The method name to call (priority defaults to 0)
     *  * An array composed of the method name to call and the priority
     *  * An array of arrays composed of the method names to call and respective
     *    priorities, or 0 if unset
     *
     * For instance:
     *
     *  * array('eventName' => 'methodName')
     *  * array('eventName' => array('methodName', $priority))
     *  * array('eventName' => array(array('methodName1', $priority), array('methodName2')))
     *
     * @return array The event names to listen to
     */
    public static function getSubscribedEvents()
    {
        return [
            DSYWebpayBundleEvents::POST_ONE_CLICK_INSCRIPTION => 'onDsywebpaybundlePostoneclickinscription',
            DSYWebpayBundleEvents::NORMAL_PAYMENT_SUCCESS => 'onDsywebpaybundleNormalpaymentsuccess',
            DSYWebpayBundleEvents::NORMAL_PAYMENT_FAILURE => 'onDsywebpaybundleNormalpaymenterror',
        ];
    }
}
