<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use dsarhoya\WebpayBundle\Interfaces\OneClickIncriptionInterface;

/**
 * OneClickInscription
 *
 * @ORM\Table(name="one_click_inscription")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\OneClickInscriptionRepository")
 */
class OneClickInscription implements OneClickIncriptionInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="userName", type="string", length=255)
     */
    private $userName;

    /**
     * @var string
     *
     * @ORM\Column(name="oneClickToken", type="string", length=255, nullable=true)
     */
    private $oneClickToken;

    /**
     * @var string
     *
     * @ORM\Column(name="oneClickAuthCode", type="string", length=255, nullable=true)
     */
    private $oneClickAuthCode;

    /**
     * @var string
     *
     * @ORM\Column(name="oneClickCreditCardType", type="string", length=255, nullable=true)
     */
    private $oneClickCreditCardType;

    /**
     * @var int
     *
     * @ORM\Column(name="oneClickLast4CardDigits", type="integer", nullable=true)
     */
    private $oneClickLast4CardDigits;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $oneClickResponseCode;

    /**
     * @var int
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $removedAt;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set email.
     *
     * @param string $email
     *
     * @return OneClickInscription
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email.
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set userName.
     *
     * @param string $userName
     *
     * @return OneClickInscription
     */
    public function setUserName($userName)
    {
        $this->userName = $userName;

        return $this;
    }

    /**
     * Get userName.
     *
     * @return string
     */
    public function getUserName()
    {
        return $this->userName;
    }

    /**
     * Set oneClickToken.
     *
     * @param string $oneClickToken
     *
     * @return OneClickInscription
     */
    public function setOneClickToken($oneClickToken)
    {
        $this->oneClickToken = $oneClickToken;

        return $this;
    }

    /**
     * Get oneClickToken.
     *
     * @return string
     */
    public function getOneClickToken()
    {
        return $this->oneClickToken;
    }

    /**
     * Set oneClickAuthCode.
     *
     * @param string $oneClickAuthCode
     *
     * @return OneClickInscription
     */
    public function setOneClickAuthCode($oneClickAuthCode)
    {
        $this->oneClickAuthCode = $oneClickAuthCode;

        return $this;
    }

    /**
     * Get oneClickAuthCode.
     *
     * @return string
     */
    public function getOneClickAuthCode()
    {
        return $this->oneClickAuthCode;
    }

    /**
     * Set oneClickCreditCardType.
     *
     * @param string $oneClickCreditCardType
     *
     * @return OneClickInscription
     */
    public function setOneClickCreditCardType($oneClickCreditCardType)
    {
        $this->oneClickCreditCardType = $oneClickCreditCardType;

        return $this;
    }

    /**
     * Get oneClickCreditCardType.
     *
     * @return string
     */
    public function getOneClickCreditCardType()
    {
        return $this->oneClickCreditCardType;
    }

    /**
     * Set oneClickLast4CardDigits.
     *
     * @param int $oneClickLast4CardDigits
     *
     * @return OneClickInscription
     */
    public function setOneClickLast4CardDigits($oneClickLast4CardDigits)
    {
        $this->oneClickLast4CardDigits = $oneClickLast4CardDigits;

        return $this;
    }

    /**
     * Get oneClickLast4CardDigits.
     *
     * @return int
     */
    public function getOneClickLast4CardDigits()
    {
        return $this->oneClickLast4CardDigits;
    }
    
    public function getWebpayType(){
        return 'oneclick';
    }

    /**
     * Set oneClickResponseCode.
     *
     * @param int|null $oneClickResponseCode
     *
     * @return OneClickInscription
     */
    public function setOneClickResponseCode($oneClickResponseCode = null)
    {
        $this->oneClickResponseCode = $oneClickResponseCode;

        return $this;
    }

    /**
     * Get oneClickResponseCode.
     *
     * @return int|null
     */
    public function getOneClickResponseCode()
    {
        return $this->oneClickResponseCode;
    }

    /**
     * Set removedAt.
     *
     * @param \DateTime|null $removedAt
     *
     * @return OneClickInscription
     */
    public function setRemovedAt($removedAt = null)
    {
        $this->removedAt = $removedAt;

        return $this;
    }

    /**
     * Get removedAt.
     *
     * @return \DateTime|null
     */
    public function getRemovedAt()
    {
        return $this->removedAt;
    }
    
    public function getStatus(){
        return null === $this->getRemovedAt() ? self::STATUS_ENABLED : self::STATUS_DISABLED;
    }
}
