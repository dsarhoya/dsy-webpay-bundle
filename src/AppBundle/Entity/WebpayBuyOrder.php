<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use dsarhoya\WebpayBundle\Interfaces\WebpayBuyOrderInterface;

/**
 * WebpayBuyOrder.
 *
 * @ORM\Table(name="webpay_buy_order")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\WebpayBuyOrderRepository")
 */
class WebpayBuyOrder implements WebpayBuyOrderInterface
{
    use \dsarhoya\WebpayBundle\Traits\WebpayBuyOrderNormalTrait;
    public const ONE_CLICK_ORDER_STARTS_FROM = 10000;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var float
     *
     * @ORM\Column(name="amount", type="float")
     */
    private $amount;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=255)
     */
    private $code;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=255)
     */
    private $status = WebpayBuyOrderInterface::STATUS_PENDING;

    /**
     * @var string
     *
     * @ORM\Column(name="userEmail", type="string", length=255, nullable=true)
     */
    private $userEmail;

    /**
     * @var string
     *
     * @ORM\Column(name="username", type="string", length=255, nullable=true)
     */
    private $username;

    /**
     * @var string
     *
     * @ORM\Column(name="userToken", type="string", length=255, nullable=true)
     */
    private $userToken;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $transactionToken;

    /**
     * @var string|null
     *
     * @ORM\Column(name="creditCardType", type="string", length=255, nullable=true)
     */
    private $creditCardType;

    /**
     * @var string|null
     *
     * @ORM\Column(name="transactionId", type="string", length=255, nullable=true)
     */
    private $transactionId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="creditCardEndingNumbers", type="integer", nullable=true)
     */
    private $creditCardEndingNumbers;

    /**
     * @var string|null
     *
     * @ORM\Column(name="authorizationCode", type="string", length=255, nullable=true)
     */
    private $authorizationCode;

    /**
     * @var string|null
     *
     * @ORM\Column(name="paymentTypeCode", type="string", length=255, nullable=true)
     */
    private $paymentTypeCode;

    /**
     * @var string|null
     *
     * @ORM\Column(name="responseCode", type="string", length=255, nullable=true)
     */
    private $responseCode;

    /**
     * @var int
     *
     * @ORM\Column(name="sharesNumber", type="integer", unique=false, nullable=true)
     */
    private $sharesNumber;

    /**
     * @var int
     *
     * @ORM\Column(name="sharesAmount", type="float", unique=false, nullable=true)
     */
    private $sharesAmount;

    /**
     * @var string|null
     *
     * @ORM\Column(name="vci", type="string", length=255, nullable=true)
     */
    private $vci;

    /**
     * @var string|null
     *
     * @ORM\Column(name="transactionDateString", type="string", length=255, nullable=true)
     */
    private $transactionDateString;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $failStatus;

    /**
     * @ORM\ManyToOne(targetEntity="OneClickInscription")
     */
    private $inscription;

    /**
     * @ORM\Column(type="string", nullable=false)
     *
     * @var string
     */
    protected $webpayType = 'oneclick';

    /**
     * @ORM\Column(type="bigint", nullable=true)
     *
     * @var int
     */
    protected $reverseCode;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set amount.
     *
     * @param float $amount
     *
     * @return WebpayBuyOrder
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount.
     *
     * @return float
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set code.
     *
     * @param string $code
     *
     * @return WebpayBuyOrder
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code.
     *
     * @return string
     */
    public function getCode()
    {
        return self::ONE_CLICK_ORDER_STARTS_FROM + $this->getId();
    }

    /**
     * Set status.
     *
     * @param string $status
     *
     * @return WebpayBuyOrder
     */
    public function setStatus($status)
    {
        $statusMap = self::getStatusMap();

        if (!in_array($status, array_keys($statusMap))) {
            throw new \Exception('Unknown status');
        }

        if (self::STATUS_REVERSED === $status && self::TRANSACCION_TYPE_ONECLICK !== $this->getWebpayType()) {
            throw new \Exception('Order cannot be reversed');
        }

        $this->status = $status;

        return $this;
    }

    /**
     * Get status.
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set userEmail.
     *
     * @param string|null $userEmail
     *
     * @return WebpayBuyOrder
     */
    public function setUserEmail($userEmail = null)
    {
        $this->userEmail = $userEmail;

        return $this;
    }

    /**
     * Get userEmail.
     *
     * @return string|null
     */
    public function getUserEmail()
    {
        return $this->userEmail;
    }

    /**
     * Set username.
     *
     * @param string $username
     *
     * @return WebpayBuyOrder
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get username.
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set userToken.
     *
     * @param string $userToken
     *
     * @return WebpayBuyOrder
     */
    public function setUserToken($userToken)
    {
        $this->userToken = $userToken;

        return $this;
    }

    /**
     * Get userToken.
     *
     * @return string
     */
    public function getUserToken()
    {
        return $this->userToken;
    }

    /**
     * Set transactionToken.
     *
     * @param string $transactionToken
     *
     * @return self
     */
    public function setTransactionToken($transactionToken)
    {
        $this->transactionToken = $transactionToken;

        return $this;
    }

    /**
     * Get transactionToken.
     *
     * @return string
     */
    public function getTransactionToken()
    {
        return $this->transactionToken;
    }

    /**
     * Set creditCardType.
     *
     * @param string|null $creditCardType
     *
     * @return WebpayBuyOrder
     */
    public function setCreditCardType($creditCardType = null)
    {
        $this->creditCardType = $creditCardType;

        return $this;
    }

    /**
     * Get creditCardType.
     *
     * @return string|null
     */
    public function getCreditCardType()
    {
        return $this->creditCardType;
    }

    /**
     * Set transactionId.
     *
     * @param string|null $transactionId
     *
     * @return WebpayBuyOrder
     */
    public function setTransactionId($transactionId = null)
    {
        $this->transactionId = $transactionId;

        return $this;
    }

    /**
     * Get transactionId.
     *
     * @return string|null
     */
    public function getTransactionId()
    {
        return $this->transactionId;
    }

    /**
     * Set creditCardEndingNumbers.
     *
     * @param int|null $creditCardEndingNumbers
     *
     * @return WebpayBuyOrder
     */
    public function setCreditCardEndingNumbers($creditCardEndingNumbers = null)
    {
        $this->creditCardEndingNumbers = $creditCardEndingNumbers;

        return $this;
    }

    /**
     * Get creditCardEndingNumbers.
     *
     * @return int|null
     */
    public function getCreditCardEndingNumbers()
    {
        return $this->creditCardEndingNumbers;
    }

    /**
     * Set authorizationCode.
     *
     * @param string|null $authorizationCode
     *
     * @return WebpayBuyOrder
     */
    public function setAuthorizationCode($authorizationCode = null)
    {
        $this->authorizationCode = $authorizationCode;

        return $this;
    }

    /**
     * Get authorizationCode.
     *
     * @return string|null
     */
    public function getAuthorizationCode()
    {
        return $this->authorizationCode;
    }

    /**
     * Set paymentTypeCode.
     *
     * @param string|null $paymentTypeCode
     *
     * @return WebpayBuyOrder
     */
    public function setPaymentTypeCode($paymentTypeCode = null)
    {
        $this->paymentTypeCode = $paymentTypeCode;

        return $this;
    }

    /**
     * Get paymentTypeCode.
     *
     * @return string|null
     */
    public function getPaymentTypeCode()
    {
        return $this->paymentTypeCode;
    }

    /**
     * Set responseCode.
     *
     * @param string|null $responseCode
     *
     * @return WebpayBuyOrder
     */
    public function setResponseCode($responseCode = null)
    {
        $this->responseCode = $responseCode;

        return $this;
    }

    /**
     * Get responseCode.
     *
     * @return string|null
     */
    public function getResponseCode()
    {
        return $this->responseCode;
    }

    /**
     * Set sharesNumber.
     *
     * @param int|null $sharesNumber
     *
     * @return WebpayBuyOrder
     */
    public function setSharesNumber($sharesNumber = null)
    {
        $this->sharesNumber = $sharesNumber;

        return $this;
    }

    /**
     * Get sharesNumber.
     *
     * @return int|null
     */
    public function getSharesNumber()
    {
        return $this->sharesNumber;
    }

    /**
     * Set sharesAmount.
     *
     * @param float|null $sharesAmount
     *
     * @return WebpayBuyOrder
     */
    public function setSharesAmount($sharesAmount = null)
    {
        $this->sharesAmount = $sharesAmount;

        return $this;
    }

    /**
     * Get sharesAmount.
     *
     * @return float|null
     */
    public function getSharesAmount()
    {
        return $this->sharesAmount;
    }

    /**
     * Set vci.
     *
     * @param string|null $vci
     *
     * @return WebpayBuyOrder
     */
    public function setVci($vci = null)
    {
        $this->vci = $vci;

        return $this;
    }

    /**
     * Get vci.
     *
     * @return string|null
     */
    public function getVci()
    {
        return $this->vci;
    }

    /**
     * Set transactionDateString.
     *
     * @param string|null $transactionDateString
     *
     * @return WebpayBuyOrder
     */
    public function setTransactionDateString($transactionDateString = null)
    {
        $this->transactionDateString = $transactionDateString;

        return $this;
    }

    /**
     * Get transactionDateString.
     *
     * @return string|null
     */
    public function getTransactionDateString()
    {
        return $this->transactionDateString;
    }

    /**
     * Set inscription.
     *
     * @param \AppBundle\Entity\OneClickInscription|null $inscription
     *
     * @return WebpayBuyOrder
     */
    public function setInscription(OneClickInscription $inscription = null)
    {
        $this->inscription = $inscription;

        return $this;
    }

    /**
     * Get inscription.
     *
     * @return \AppBundle\Entity\OneClickInscription|null
     */
    public function getInscription()
    {
        return $this->inscription;
    }

    /**
     * Set failStatus.
     *
     * @param string|null $failStatus
     *
     * @return WebpayBuyOrder
     */
    public function setFailStatus($failStatus = null)
    {
        $this->failStatus = $failStatus;

        return $this;
    }

    /**
     * Get failStatus.
     *
     * @return string|null
     */
    public function getFailStatus()
    {
        return $this->failStatus;
    }

    public function getWebpayType()
    {
        return $this->webpayType;
    }

    /**
     * Set webpayType.
     *
     * @param string $webpayType
     *
     * @return WebpayBuyOrder
     */
    public function setWebpayType($webpayType)
    {
        $this->webpayType = $webpayType;

        return $this;
    }

    /**
     * Set reverseCode.
     *
     * @param int|null $reverseCode
     *
     * @return WebpayBuyOrder
     */
    public function setReverseCode($reverseCode = null)
    {
        $this->reverseCode = $reverseCode;

        return $this;
    }

    /**
     * Get reverseCode.
     *
     * @return int|null
     */
    public function getReverseCode()
    {
        return $this->reverseCode;
    }
}
