<?php

namespace AppBundle\Controller;

use AppBundle\Entity\OneClickInscription;
use AppBundle\Entity\WebpayBuyOrder;
use AppBundle\Form\OneClickChargeType;
use AppBundle\Form\OneClickInscriptionType;
use AppBundle\Form\WebpayChargeType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', [
        ]);
    }

    /**
     * @Route("/payments", name="payments")
     */
    public function paymentsAction(Request $request)
    {
        $order = new WebpayBuyOrder();
        $order->setCode(md5(time())); //Esto es solamente porque no puede ser null
        $form = $this->createForm(WebpayChargeType::class, $order);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->persist($order);
            $this->getDoctrine()->getManager()->flush($order);

            //Esto porque el getCode usa un número mínimo porque webpay no permite ordenes con números chicos.
            $order->setCode($order->getCode());
            $this->getDoctrine()->getManager()->flush($order);

            return $this->redirectToRoute('webpay_normal_start', ['orderId' => $order->getId()]);
        }

        return $this->render('default/payments.html.twig', [
            // 'inscriptions' => $this->getDoctrine()->getRepository(OneClickInscription::class)->findAll(),
            'payments' => [],
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/inscriptions", name="inscriptions")
     */
    public function inscriptionsAction(Request $request)
    {
        $inscription = new OneClickInscription();
        $form = $this->createForm(OneClickInscriptionType::class, $inscription);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->persist($inscription);
            $this->getDoctrine()->getManager()->flush($inscription);

            return $this->redirectToRoute('webpay_oneclick_inscription', ['oneClickInscriptionEntityId' => $inscription->getId()]);
        }
        // replace this example code with whatever you need
        return $this->render('default/inscriptions.html.twig', [
            'inscriptions' => $this->getDoctrine()->getRepository(OneClickInscription::class)->findAll(),
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/inscription/{id}/remove", name="remove_inscription")
     */
    public function removeInscriptionAction(Request $request, OneClickInscription $inscription)
    {
        $res = $this->get('dsy.webpay.service')->removeOneclickInscription($inscription);
        if (true === $res) {
            $this->addFlash('success', 'Reversa exitosa');
        } else {
            $this->addFlash('error', 'Reversa fallida');
        }

        return $this->redirectToRoute('inscriptions');
    }

    /**
     * @Route("/inscription/{id}", name="inscription")
     */
    public function inscriptionAction(Request $request, OneClickInscription $inscription)
    {
        $order = new WebpayBuyOrder();
        $order->setUsername($inscription->getUserName());
        $order->setUserToken($inscription->getOneClickToken());
        $order->setStatus(WebpayBuyOrder::STATUS_PENDING);
        $order->setInscription($inscription);
        $form = $this->createForm(OneClickChargeType::class, $order);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            //$order->setCode(time());
            $this->getDoctrine()->getManager()->persist($order);
            $this->getDoctrine()->getManager()->flush($order);
            try {
                $res = $this->get('dsy.webpay.service')->authorizeOneClick($order);
            } catch (\Exception $e) {
            }

            return $this->redirectToRoute('inscription', ['id' => $inscription->getId()]);
        }

        return $this->render('default/inscription.html.twig', [
            'inscription' => $inscription,
            'form' => $form->createView(),
            'orders' => $this->getDoctrine()->getRepository(WebpayBuyOrder::class)->findByInscription($inscription),
        ]);
    }

    /**
     * @Route("/inscription/{id}/reverse/{order}", name="reverse_order")
     */
    public function reverseAction(Request $request, OneClickInscription $inscription, WebpayBuyOrder $order)
    {
        $res = $this->get('dsy.webpay.service')->reverseOneClickBuyOrder($order);
        if (true === $res) {
            $this->addFlash('success', 'Reversa exitosa');
        } else {
            $this->addFlash('error', 'Reversa fallida');
        }

        return $this->redirectToRoute('inscription', ['id' => $inscription->getId()]);
    }
}
