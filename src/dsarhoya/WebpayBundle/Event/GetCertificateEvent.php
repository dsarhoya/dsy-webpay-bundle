<?php

namespace dsarhoya\WebpayBundle\Event;

use Symfony\Component\EventDispatcher\Event;

/**
 * Description of GetCertificateEvent.
 *
 * @author snake77se <yosip.curiel@dsarhoya.cl>
 */
class GetCertificateEvent extends Event
{
    /**
     * @var string
     */
    protected $key;

    /**
     * @var string
     */
    protected $crt;

    /**
     * @var string
     */
    protected $pem;

    /**
     * @var string
     */
    protected $options;

    public function __construct($options = [])
    {
        $this->options = $options;
    }

    /**
     * @param string $key
     *
     * @return $this
     */
    public function setKey($key)
    {
        $this->key = $key;

        return $this;
    }

    /**
     * Get Key.
     *
     * @return string
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * @param string $crt
     *
     * @return $this
     */
    public function setCrt($crt)
    {
        $this->crt = $crt;

        return $this;
    }

    /**
     * Get Crt.
     *
     * @return string
     */
    public function getCrt()
    {
        return $this->crt;
    }

    /**
     * @param string $pem
     *
     * @return $this
     */
    public function setPem($pem)
    {
        $this->pem = $pem;

        return $this;
    }

    /**
     * Get Pem.
     *
     * @return string
     */
    public function getPem()
    {
        return $this->pem;
    }

    public function getOptions()
    {
        return $this->options;
    }
}
