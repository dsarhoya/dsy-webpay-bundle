<?php

namespace dsarhoya\WebpayBundle\Event;

/**
 * Description of DSYWebpayBundleEvents.
 *
 * @author snake77se <yosip.curiel@dsarhoya.cl>
 */
final class DSYWebpayBundleEvents
{
    //Event Response inscription oneclick
    /** @Event("Symfony\Component\HttpKernel\Event\GetResponseForControllerResultEvent") */
    const POST_ONE_CLICK_INSCRIPTION = 'dsy_webpay_bundle.post_one_click_inscription';

    //Event Response inscription oneclick
    /** @Event("Symfony\Component\HttpKernel\Event\GetResponseForControllerResultEvent") */
    const POST_ONE_CLICK_AUTHORIZATION = 'dsy_webpay_bundle.post_one_click_authorization';

    //Event get Certificate webpay
    /** @Event("dsarhoya\WebpayBundle\Event\GetCertificateEvent") */
    const GET_CERTIFICATE = 'dsy_webpay_bundle.get_certificate';

    /** @Event("Symfony\Component\HttpKernel\Event\GetResponseForControllerResultEvent") */
    const NORMAL_PAYMENT_PRE_ACKNOWLEDGE_TRANSACTION = 'dsy_webpay_bundle.normal_pre_acknowledge_transaction';

    /** @Event("Symfony\Component\HttpKernel\Event\GetResponseForControllerResultEvent") */
    const NORMAL_PAYMENT_POST_ACKNOWLEDGE_TRANSACTION = 'dsy_webpay_bundle.normal_post_acknowledge_transaction';

    /** @Event("Symfony\Component\HttpKernel\Event\GetResponseForControllerResultEvent") */
    const NORMAL_PAYMENT_SUCCESS = 'dsy_webpay_bundle.normal_payment_success';

    /** @Event("Symfony\Component\HttpKernel\Event\GetResponseForControllerResultEvent") */
    const NORMAL_PAYMENT_FAILURE = 'dsy_webpay_bundle.normal_payment_failure';
}
