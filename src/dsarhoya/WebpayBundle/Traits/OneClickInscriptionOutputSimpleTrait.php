<?php

namespace dsarhoya\WebpayBundle\Traits;

use Doctrine\ORM\Mapping as ORM;

/**
 * @author snake77se <yosip.curiel@dsarhoya.cl>
 */
trait OneClickInscriptionOutputSimpleTrait
{
    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $username;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $oneClickToken;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $oneClickAuthCode;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $oneClickCreditCardType;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $oneClickLast4CardDigits;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $oneClickResponseCode;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $removedAt;

    /**
     * Set username.
     *
     * @param string $username
     *
     * @return self
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get username.
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set oneClickToken.
     *
     * @param string $oneClickToken
     *
     * @return self
     */
    public function setOneClickToken($oneClickToken)
    {
        $this->oneClickToken = $oneClickToken;

        return $this;
    }

    /**
     * Get oneClickToken.
     *
     * @return string
     */
    public function getOneClickToken()
    {
        return $this->oneClickToken;
    }

    /**
     * Set oneClickAuthCode.
     *
     * @param string $oneClickAuthCode
     *
     * @return self
     */
    public function setOneClickAuthCode($oneClickAuthCode)
    {
        $this->oneClickAuthCode = $oneClickAuthCode;

        return $this;
    }

    /**
     * Get oneClickAuthCode.
     *
     * @return string
     */
    public function getOneClickAuthCode()
    {
        return $this->oneClickAuthCode;
    }

    /**
     * Set oneClickCreditCardType.
     *
     * @param string $oneClickCreditCardType
     *
     * @return self
     */
    public function setOneClickCreditCardType($oneClickCreditCardType)
    {
        $this->oneClickCreditCardType = $oneClickCreditCardType;

        return $this;
    }

    /**
     * Get oneClickCreditCardType.
     *
     * @return string
     */
    public function getOneClickCreditCardType()
    {
        return $this->oneClickCreditCardType;
    }

    /**
     * Set oneClickLast4CardDigits.
     *
     * @param string $oneClickLast4CardDigits
     *
     * @return self
     */
    public function setOneClickLast4CardDigits($oneClickLast4CardDigits)
    {
        $this->oneClickLast4CardDigits = $oneClickLast4CardDigits;

        return $this;
    }

    /**
     * Get oneClickLast4CardDigits.
     *
     * @return string
     */
    public function getOneClickLast4CardDigits()
    {
        return $this->oneClickLast4CardDigits;
    }

    /**
     * Set oneClickResponseCode.
     *
     * @param string
     *
     * @return self
     */
    public function setOneClickResponseCode($oneClickResponseCode)
    {
        $this->oneClickResponseCode = $oneClickResponseCode;

        return $this;
    }

    /**
     * Get oneClickResponseCode.
     *
     * @return string
     */
    public function getOneClickResponseCode()
    {
        return $this->oneClickResponseCode;
    }

    /**
     * Get webpayType.
     *
     * @return string
     */
    public function getWebpayType()
    {
        return WebpayBuyOrderInterface::TRANSACCION_TYPE_ONECLICK;
    }

    /**
     * Set removedAt.
     *
     * @param \DateTime|null $removedAt
     *
     * @return self
     */
    public function setRemovedAt($removedAt = null)
    {
        $this->removedAt = $removedAt;

        return $this;
    }

    /**
     * Get removedAt.
     *
     * @return \DateTime|null
     */
    public function getRemovedAt()
    {
        return $this->removedAt;
    }
}
