<?php

namespace dsarhoya\WebpayBundle\Traits;

use dsarhoya\WebpayBundle\Interfaces\WebpayBuyOrderInterface;

/**
 * @author mati
 */
trait WebpayBuyOrderNormalTrait
{
    /**
     * get Failure description.
     *
     * @return string
     */
    public function getFailStatusDescription()
    {
        switch ($this->getFailStatus()) {
            case self::FAIL_STATUS_CERTIFICATE_VALIDATION:
                return 'Ocurrió un error durante su transacción. No hemos podido realizar el cobro';
            case self::FAIL_STATUS_NO_VCI:
                return 'Ocurrió un error durante su transacción. No hemos podido realizar el cobro';
            case self::FAIL_STATUS_ALREADY_PAYED:
                return 'La orden ya se encuentra pagada. Por favor contáctese con nosotros.';
            case self::FAIL_STATUS_GENERAL:
                return 'La transacción no fue autorizada. No hemos podido realizar el cobro.';
            case self::FAIL_STATUS_DECLINED_BY_USER:
                return 'Proceso de pago anulado por el cliente.';
            case self::FAIL_STATUS_TIMEOUT:
                return 'Se terminó el tiempo.';
        }

        return 'Proceso exitoso.';
    }

    /**
     * getPayment type.
     *
     * @return string
     */
    public function getPaymentType()
    {
        return ('VD' === $this->getPaymentTypeCode()) ? 'Débito' : 'Crédito';
    }

    /**
     * @return string
     */
    public function getSharesDescription()
    {
        switch ($this->getPaymentTypeCode()) {
            case 'VD':
                return 'Venta débito';
            case 'VN':
                return 'Sin coutas';
            case 'VC':
                return 'Cuotas normales';
        }

        return 'Sin interés';
    }

    /**
     * @return string
     */
    public function getStatusWithCorrection()
    {
        if (null === $this->getAuthorizationCode() && self::STATUS_PAYED === $this->getStatus()) {
            if (self::FAIL_STATUS_NONE === $this->getFailStatus()) {
                return self::STATUS_PENDING;
            } else {
                return self::STATUS_FAILED;
            }
        }

        return $this->getStatus();
    }

    /**
     * @return \DateTime
     */
    public function getTransactionDate()
    {
        if (false === $date = \DateTime::createFromFormat('Y-m-d\TH:i:s.uP', $this->getTransactionDateString())) {
            if (method_exists($this, 'getCreatedAt')) {
                return $this->getCreatedAt();
            }

            return null;
        }

        return $date;
    }

    /**
     * @return array
     */
    public static function getStatusMap()
    {
        return [
            WebpayBuyOrderInterface::STATUS_PENDING => 'Pendiente',
            WebpayBuyOrderInterface::STATUS_PROCESSING => 'Procesando',
            WebpayBuyOrderInterface::STATUS_PAYED => 'Pagada',
            WebpayBuyOrderInterface::STATUS_FAILED => 'Fallida',
            WebpayBuyOrderInterface::STATUS_REVERSED => 'Reversada',
        ];
    }
}
