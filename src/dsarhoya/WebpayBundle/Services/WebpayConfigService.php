<?php

namespace dsarhoya\WebpayBundle\Services;

use dsarhoya\WebpayBundle\Event\DSYWebpayBundleEvents;
use dsarhoya\WebpayBundle\Event\GetCertificateEvent;
use dsarhoya\WebpayBundle\Interfaces\WebpayBuyOrderInterface;
use Freshwork\Transbank\CertificationBag;
use Freshwork\Transbank\CertificationBagFactory;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpKernel\KernelInterface;

/**
 * Description of WebpayConfigService.
 *
 * @author mati
 */
class WebpayConfigService
{
    /**
     * @var \Symfony\Component\HttpKernel\KernelInterface;
     */
    private $kernel;

    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    private $certsConfig;
    private $apiConfig;
    private $env;

    /**
     * Constructor.
     *
     * @param TraceableEventDispatcher $eventDispacher
     */
    public function __construct(EventDispatcherInterface $eventDispacher, KernelInterface $kernel, $certsConfig, $apiConfig, $env)
    {
        $this->eventDispatcher = $eventDispacher;
        $this->kernel = $kernel;
        $this->certsConfig = $certsConfig;
        $this->apiConfig = $apiConfig;
        $this->env = $env;
    }

    /**
     * Get CertificateBag Production or null.
     *
     * @return CertificationBag|null
     */
    public function getCertificationBag($type, $options = [])
    {
        $env = 'integration';
        if ('prod' === $this->kernel->getEnvironment()) {
            $env = 'production';
        }

        if (isset($this->certsConfig['force_env'])) {
            if ('integration' === $this->certsConfig['force_env']) {
                $env = $this->certsConfig['force_env'];
            }
            if ('production' === $this->certsConfig['force_env']) {
                $env = $this->certsConfig['force_env'];
            }
        }

        if ('production' === $env) {
            /* @var $event \dsarhoya\WebpayBundle\Event\GetCertificateEvent */
            $eventInit = new GetCertificateEvent(array_merge(['type' => $type], $options));
            $eventCertificate = $this->eventDispatcher->dispatch(DSYWebpayBundleEvents::GET_CERTIFICATE, $eventInit);
            if (null !== $eventCertificate->getKey() && null !== $eventCertificate->getCrt() && null !== $eventCertificate->getPem()) {
                return CertificationBagFactory::production("{$eventCertificate->getKey()}", "{$eventCertificate->getCrt()}", "{$eventCertificate->getPem()}");
            }
            $certsDir = $this->kernel->getRootDir().'/../certs';
            if (null !== $this->certsConfig && isset($this->certsConfig['prod'])) {
                $prodCerts = $this->certsConfig['prod'];

                return CertificationBagFactory::production(
                    "{$certsDir}/{$prodCerts['key_name']}", "{$certsDir}/{$prodCerts['crt_name']}", "{$certsDir}/{$prodCerts['pem_name']}"
                );
            }
            throw new \Exception('No se pudo obtener los certificados de producción.');
        } else {
            if (WebpayBuyOrderInterface::TRANSACCION_TYPE_NORMAL === $type) {
                return CertificationBagFactory::integrationWebpayNormal();
            } elseif (WebpayBuyOrderInterface::TRANSACCION_TYPE_ONECLICK === $type) {
                return CertificationBagFactory::integrationOneClick();
            } else {
                throw new \Exception("Webpay type '{$type}' no implementado");
            }
        }
        throw new \Exception("Webpay type '{$type}' no implementado");
    }

    /**
     * @return void
     */
    public function geApiConfig()
    {
        if (null === $this->apiConfig) {
            throw new \Exception('No existe la configuración de la api');
        }

        //TODO pasar esto a un modelo
        return [
            'commerce_code' => $this->apiConfig['commerce_code'],
            'api_key' => $this->apiConfig['api_key'],
        ];
    }

    /**
     * @return bool
     */
    public function isProd()
    {
        return 'prod' === $this->env;
    }

    public function getPreferredIntegration()
    {
        return $this->apiConfig ? 'api' : 'certs';
    }
}
