<?php

namespace dsarhoya\WebpayBundle\Services;

use Freshwork\Transbank\Log\LoggerFactory;
use Freshwork\Transbank\Log\TransbankCertificationLogger;
use Freshwork\Transbank\TransbankServiceFactory;
use dsarhoya\WebpayBundle\Interfaces\WebpayBuyOrderInterface;
use dsarhoya\WebpayBundle\Interfaces\OneClickIncriptionInterface;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpKernel\KernelInterface;
use Freshwork\Transbank\CertificationBag;
use Transbank\Webpay\WebpayPlus\Transaction;
use Transbank\Webpay\WebpayPlus;

/**
 * Description of WebpayService.
 *
 * @author snake77se <yosip.curiel@dsarhoya.cl>
 */
class WebpayService
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;

    /**
     * @var \Symfony\Component\HttpKernel\KernelInterface;
     */
    private $kernel;

    /**
     * @var WebpayConfigService
     */
    private $configService;

    /**
     * Constructor.
     *
     * @param TraceableEventDispatcher $eventDispacher
     */
    public function __construct(EntityManager $em, KernelInterface $kernel, WebpayConfigService $configService)
    {
        $this->em = $em;
        $this->kernel = $kernel;
        $this->configService = $configService;
    }

    /**
     * Get CertificateBag Production or null.
     *
     * @return null|CertificationBag
     */
    public function getCertificationBag($type, $options = [])
    {
        return $this->configService->getCertificationBag($type, $options);
    }

    /**
     * Get Client Webpay.
     *
     * @param string $wpType
     *
     * @return \Freshwork\Transbank\WebpayNormal|\Freshwork\Transbank\WebpayOneClick|\Freshwork\Transbank\WebpayPatPass
     */
    public function getWebpayClient($wpType, $options = [])
    {
        LoggerFactory::setLogger(new TransbankCertificationLogger($this->kernel->getLogDir()));

        $certificationBag = $this->getCertificationBag($wpType, $options);

        if (WebpayBuyOrderInterface::TRANSACCION_TYPE_NORMAL === $wpType) {
            return TransbankServiceFactory::normal($certificationBag);
        } elseif (WebpayBuyOrderInterface::TRANSACCION_TYPE_ONECLICK === $wpType) {
            return TransbankServiceFactory::oneclick($certificationBag);
        } else {
            throw new \Exception("Webpay type '{$wpType}' no implementado");
        }
    }

    /**
     * De acá en adelante es versión nueva de webpay con api
     */

    /**
     * @return \Transbank\Webpay\WebpayPlus\Transaction;
     */
    public function getTransaction($wpType, $options = [])
    {
        if (WebpayBuyOrderInterface::TRANSACCION_TYPE_NORMAL !== $wpType) {
            throw new \Exception("Tipo $wpType no implementado");
        }

        $config = $this->configService->geApiConfig();

        if ($this->configService->isProd()) {
            WebpayPlus::configureForProduction($config['commerce_code'], $config['api_key']);
        }else{
            WebpayPlus::configureForIntegration($config['commerce_code'], $config['api_key']);
        }
        
        $transaction = new Transaction();

        return $transaction;
    }


    /**
     * De acá en adelante es oneclick
     */

    /**
     * @param WebpayBuyOrderInterface $order
     */
    public function authorizeOneClick(WebpayBuyOrderInterface $order)
    {
        /* @var $oneClick \Freshwork\Transbank\WebpayOneClick */
        $oneClick = $this->getWebpayClient(WebpayBuyOrderInterface::TRANSACCION_TYPE_ONECLICK, ['order' => $order]);

        try {
            /* @var $response \Freshwork\Transbank\WebpayOneClick\oneClickPayOutput */
            $response = $oneClick->authorize($order->getAmount(), $order->getCode(), $order->getUsername(), $order->getUserToken());
            $order->setAuthorizationCode($response->authorizationCode);
            $order->setCreditCardEndingNumbers($response->last4CardDigits);
            $order->setResponseCode($response->responseCode);
            $order->setCreditCardType($response->creditCardType);
            $order->setTransactionId($response->transactionId);
            $order->setStatus(0 === $response->responseCode ? WebpayBuyOrderInterface::STATUS_PAYED : WebpayBuyOrderInterface::STATUS_FAILED);
        } catch (\SoapFault $f) {
            //throw $th;
            $order->setStatus(WebpayBuyOrderInterface::STATUS_FAILED);
            // dump($f);
            // die;
        }

        $this->em->flush($order);

        return $order;
    }

    /**
     * @param WebpayBuyOrderInterface $order [description]
     *
     * @return bool reverse code
     */
    public function reverseOneClickBuyOrder(WebpayBuyOrderInterface $order)
    {
        if (WebpayBuyOrderInterface::TRANSACCION_TYPE_ONECLICK !== $order->getWebpayType()) {
            throw new \Exception('Order cannot be reversed, it is not one click');
        }

        $oneClick = $this->getWebpayClient(WebpayBuyOrderInterface::TRANSACCION_TYPE_ONECLICK, ['order' => $order]);
        $response = $oneClick->codeReverseOneClick($order->getCode());

        if (null !== $response->reverseCode) {
            $order->setReverseCode($response->reverseCode);
            $order->setStatus(WebpayBuyOrderInterface::STATUS_REVERSED);
            $this->em->flush($order);

            return true;
        }

        return false;
    }

    /**
     * @param OneClickIncriptionInterface $inscription
     *
     * @return bool
     */
    public function removeOneclickInscription(OneClickIncriptionInterface $inscription)
    {
        $oneClick = $this->getWebpayClient(WebpayBuyOrderInterface::TRANSACCION_TYPE_ONECLICK, ['inscription' => $inscription]);
        $res = $oneClick->removeUser($inscription->getOneClickToken(), $inscription->getUserName());
        if (true === $res) {
            $inscription->setRemovedAt(new \DateTime('now'));
            $this->em->flush($inscription);
        }

        return $res;
    }
}
