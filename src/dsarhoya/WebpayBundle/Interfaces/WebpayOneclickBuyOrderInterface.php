<?php

namespace dsarhoya\WebpayBundle\Interfaces;

/**
 * @author snake77se <yosip.curiel@dsarhoya.cl>
 */
interface WebpayOneclickBuyOrderInterface
{
    /* OneClick */

    /**
     * @return string
     */
    public function getUserEmail();

    /**
     * @param string $userEmail
     *
     * @return self
     */
    public function setUserEmail($userEmail);

    /**
     * @return string
     */
    public function getUsername();

    /**
     * @param string $username
     *
     * @return self
     */
    public function setUsername($username);

    /**
     * @return string
     */
    public function getUserToken();

    /**
     * @param string $userToken
     *
     * @return self
     */
    public function setUserToken($userToken);

    /**
     * @return string
     */
    public function getCreditCardType();

    /**
     * @param string $creditCardType
     *
     * @return self
     */
    public function setCreditCardType($creditCardType);

    /**
     * @return string
     */
    public function getTransactionId();

    /**
     * @param string $transactionId
     *
     * @return self
     */
    public function setTransactionId($transactionId);

    /**
     * @return string
     */
    public function getReverseCode();

    /**
     * @param string $reverseCode
     *
     * @return self
     */
    public function setReverseCode($reverseCode);

    /**
     * @return string
     */
    public function getWebpayType();
}
