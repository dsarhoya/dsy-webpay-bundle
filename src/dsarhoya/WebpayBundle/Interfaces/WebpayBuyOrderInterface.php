<?php

namespace dsarhoya\WebpayBundle\Interfaces;

/**
 * @author snake77se <yosip.curiel@dsarhoya.cl>
 */
interface WebpayBuyOrderInterface
{
    public const STATUS_PENDING = 'pending';
    public const STATUS_PROCESSING = 'processing';
    public const STATUS_PAYED = 'payed';
    public const STATUS_FAILED = 'failed';
    public const STATUS_REVERSED = 'reversed';

    public const FAIL_STATUS_NONE = 'none';
    public const FAIL_STATUS_CERTIFICATE_VALIDATION = 'cert-validation';
    public const FAIL_STATUS_NO_VCI = 'no-vci';
    public const FAIL_STATUS_ALREADY_PAYED = 'a-payed';
    public const FAIL_STATUS_GENERAL = 'general';
    public const FAIL_STATUS_DECLINED_BY_USER = 'declined';
    public const FAIL_STATUS_TIMEOUT = 'timeout';

    public const VCI_SUCCESSFULL_AUTHENTICATION = 'TSY';
    public const VCI_FAILED_AUTHENTICATION = 'TSN';
    public const VCI_MAXIMUN_TIME_EXCEEDED_FOR_AUTHENTICATION = 'TO';
    public const VCI_AUTHENTICATION_ABORTED_BY_CARDHOLDER = 'ABO';
    public const VCI_INTERNAL_AUTHENTICATION_ERROR = 'U3';
    public const VCI_NOT_AUTHENTIC = '';

    public const RESPONSE_CODE_APPROVED_TRANSACTION = 0;
    public const RESPONSE_CODE_TRANSACTION_REFUSAL = -1;
    public const RESPONSE_CODE_TRANSACTION_MUST_BE_RETRYED = -2;
    public const RESPONSE_CODE_TRANSACTION_ERROR = -3;
    public const RESPONSE_CODE_REJECTTION_TRANSACTION = -4;
    public const RESPONSE_CODE_REJECTTION_BY_ERROR_OF_RATE = -5;
    public const RESPONSE_CODE_EXCEEDS_MONTHLY_MAXIMUM_QUOTA = -6;
    public const RESPONSE_CODE_EXCEEDS_DAILY_LIMIT_PER_TRANSACTION = -7;
    public const RESPONSE_CODE_UNAUTHORIZED_ITEM = -8;
    public const RESPONSE_CODE_ONE_CLICK_DAILY_LIMIT_PER_TRANSACTION_EXCEEDS = -97;
    public const RESPONSE_CODE_ONE_CLICK_AMOUNT_LIMIT_PER_TRANSACTION_EXCEEDS = -98;
    public const RESPONSE_CODE_ONE_CLICK_MAXIMUN_DAILY_PAYMENTS_EXCEEDS = -99;

    public const TRANSACCION_TYPE_NORMAL = 'normal';
    public const TRANSACCION_TYPE_ONECLICK = 'oneclick';
    // const TRANSACCION_TYPE_PATPASS = 'patpass';

    /* General */

    /**
     * @return int
     */
    public function getId();

    /**
     * @return float
     */
    public function getAmount();

    /**
     * @return self
     */
    public function setAmount($amount);

    /**
     * @return string
     */
    public function getCode();

    /**
     * @return string
     */
    public function getStatus();

    /**
     * @return self
     */
    public function setStatus($status);

    /**
     * @return string
     */
    public function getWebpayType();

    /**
     * @return \DateTime
     */
    public function getTransactionDate();

    public function setCreditCardEndingNumbers($creditCardEndingNumbers);

    public function setAuthorizationCode($authorizationCode);

    public function setPaymentTypeCode($paymentTypeCode);

    public function setResponseCode($responseCode);

    public function setSharesNumber($sharesNumber);

    public function setSharesAmount($sharesAmount);

    public function setTransactionDateString($transactionDateString);

    public function setVci($vci);

    public function getVci();

    public function getResponseCode();

    public function setFailStatus($failStatus);

    public function getTransactionToken();

    public function setTransactionToken($transactionToken);
}
