<?php

namespace dsarhoya\WebpayBundle\Interfaces;

/**
 * Se dispone de un trait para ayudar para las propiedades seters y geters de los mismo OneClickInscriptionOutput
 * "dsarhoya\WebpayBundle\traits\OneClickInscriptionOutputTrait".
 *
 * @author snake77se <yosip.curiel@dsarhoya.cl>
 */
interface OneClickIncriptionInterface
{
    const STATUS_ENABLED = 'enabled';
    const STATUS_DISABLED = 'disabled';

    //inscription

    /**
     * @return int
     */
    public function getId();

    /**
     * @return string
     */
    public function getEmail();

    /**
     * @param string $username
     *
     * @return self
     */
    public function setUsername($username);

    /**
     * @return string
     */
    public function getUsername();

    //reponse

    /**
     * @param string $oneClickToken
     *
     * @return self
     */
    public function setOneClickToken($oneClickToken);

    /**
     * @return string
     */
    public function getOneClickToken();

    /**
     * @param string $oneClickAuthCode
     *
     * @return self
     */
    public function setOneClickAuthCode($oneClickAuthCode);

    /**
     * @return string
     */
    public function getOneClickAuthCode();

    /**
     * @param string $oneClickCreditCardType
     *
     * @return self
     */
    public function setOneClickCreditCardType($oneClickCreditCardType);

    /**
     * @return string
     */
    public function getOneClickCreditCardType();

    /**
     * @param string $oneClickResponseCode
     *
     * @return self
     */
    public function setOneClickResponseCode($oneClickResponseCode);

    /**
     * @return string
     */
    public function getOneClickResponseCode();

    /**
     * @param string $oneClickLast4CardDigits
     *
     * @return self
     */
    public function setOneClickLast4CardDigits($oneClickLast4CardDigits);

    /**
     * @return string
     */
    public function getOneClickLast4CardDigits();

    /**
     * @param \DateTime $removedAt
     *
     * @return self
     */
    public function setRemovedAt($removedAt = null);

    /**
     * @return \DateTime|null
     */
    public function getRemovedAt();

    /**
     * @return string
     */
    public function getStatus();
}
