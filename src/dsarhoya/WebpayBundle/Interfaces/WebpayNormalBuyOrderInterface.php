<?php

namespace dsarhoya\WebpayBundle\Interfaces;

/**
 * WebpayNormalBuyOrderInterface.
 *
 * @author snake77se <yosip.curiel@dsarhoya.cl>
 */
interface WebpayNormalBuyOrderInterface extends WebpayBuyOrderInterface
{
    /* WebpayNormal */

    /**
     * @param string $creditCardEndingNumbers
     *
     * @return self
     */
    public function setCreditCardEndingNumbers($creditCardEndingNumbers);

    /**
     * @param string $authorizationCode
     *
     * @return self
     */
    public function setAuthorizationCode($authorizationCode);

    /**
     * @param string $paymentTypeCode
     *
     * @return self
     */
    public function setPaymentTypeCode($paymentTypeCode);

    /**
     * @param string $responseCode
     *
     * @return self
     */
    public function setResponseCode($responseCode);

    /**
     * @param string $sharesNumber
     *
     * @return self
     */
    public function setSharesNumber($sharesNumber);

    /**
     * @param string $sharesAmount
     *
     * @return self
     */
    public function setSharesAmount($sharesAmount);

    /**
     * @param string $transactionDateString
     *
     * @return self
     */
    public function setTransactionDateString($transactionDateString);

    /**
     * @param string $vci
     *
     * @return self
     */
    public function setVci($vci);

    /**
     * @param string $vci
     *
     * @return self
     */
    public function setFailStatus($failStatus);

    /**
     * @return string
     */
    public function getPaymentType();

    /**
     * @return string
     */
    public function getSharesDescription();
}
