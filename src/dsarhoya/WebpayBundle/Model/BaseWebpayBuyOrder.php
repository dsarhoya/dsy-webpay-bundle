<?php

namespace dsarhoya\WebpayBundle\Model;

use Doctrine\ORM\Mapping as ORM;
use dsarhoya\WebpayBundle\Interfaces\WebpayBuyOrderInterface;
use dsarhoya\WebpayBundle\Traits\WebpayBuyOrderNormalTrait;
// use Gedmo\Mapping\Annotation as Gedmo;
use JMS\Serializer\Annotation as JMS;

/**
 * Description of BaseWebpayBuyOrder.
 *
 * @author snake77se <yosip.curiel@dsarhoya.cl>
 */
abstract class BaseWebpayBuyOrder implements WebpayBuyOrderInterface
{
    use WebpayBuyOrderNormalTrait;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\SerializedName("id")
     * @JMS\Groups({"webpay_buy_order_list", "webpay_buy_order_detail"})
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     * @JMS\SerializedName("code")
     * @JMS\Groups({"webpay_buy_order_list", "webpay_buy_order_detail"})
     */
    protected $code;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     * @JMS\SerializedName("status")
     * @JMS\Groups({"webpay_buy_order_detail"})
     */
    protected $status = self::STATUS_PENDING;

    /**
     * @var string
     *
     * @ORM\Column(type="string", options={"default"="none"})
     * @JMS\SerializedName("fail_status")
     * @JMS\Groups({"webpay_buy_order_detail"})
     */
    protected $failStatus = self::FAIL_STATUS_NONE;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     * @JMS\SerializedName("credit_card_ending_numbers")
     * @JMS\Groups({"webpay_buy_order_detail"})
     */
    protected $creditCardEndingNumbers;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     * @JMS\SerializedName("authorization_code")
     * @JMS\Groups({"webpay_buy_order_detail"})
     */
    protected $authorizationCode;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     * @JMS\SerializedName("payment_type_code")
     * @JMS\Groups({"webpay_buy_order_detail"})
     */
    protected $paymentTypeCode;

    /**
     * @var int
     *
     * @ORM\Column(type="string", nullable=true)
     * @JMS\SerializedName("response_code")
     * @JMS\Groups({"webpay_buy_order_detail"})
     */
    protected $responseCode;

    /**
     * @var float
     *
     * @ORM\Column(type="float")
     * @JMS\SerializedName("amount")
     * @JMS\Groups({"webpay_buy_order_detail"})
     */
    protected $amount = 0.0;

    /**
     * En cuotas de...
     *
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     * @JMS\SerializedName("shares_amount")
     * @JMS\Groups({"webpaybuy_order_detail"})
     */
    protected $sharesAmount;

    /**
     * Cantidad de cuotas.
     *
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     * @JMS\SerializedName("shares_number")
     * @JMS\Groups({"webpay_buy_order_detail"})
     */
    protected $sharesNumber;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     * @JMS\SerializedName("transaction_date_string")
     * @JMS\Groups({"webpay_buy_order_detail"})
     */
    protected $transactionDateString;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     * @JMS\SerializedName("vci")
     * @JMS\Groups({"webpay_buy_order_detail"})
     */
    protected $vci;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=false)
     * @JMS\SerializedName("webpay_type")
     * @JMS\Groups({"webpay_buy_order_detail"})
     */
    protected $webpayType;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     * @JMS\SerializedName("user_Token")
     * @JMS\Groups({"webpay_buy_order_detail"})
     */
    protected $userToken;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     * @JMS\SerializedName("transaction_Token")
     * @JMS\Groups({"webpay_buy_order_detail"})
     */
    protected $transactionToken;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     * @JMS\SerializedName("user_email")
     * @JMS\Groups({"webpay_buy_order_detail"})
     */
    protected $userEmail;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     * @JMS\SerializedName("username")
     * @JMS\Groups({"webpay_buy_order_detail"})
     */
    protected $username;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     * @JMS\SerializedName("credit_card_type")
     * @JMS\Groups({"webpay_buy_order_detail"})
     */
    protected $creditCardType;

    /**
     * @var int
     *
     * @ORM\Column(type="bigint", nullable=true)
     * @JMS\SerializedName("transaction_id")
     * @JMS\Groups({"webpay_buy_order_detail"})
     */
    protected $transactionId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     * @JMS\SerializedName("created_at")
     * @JMS\Groups({"webpay_buy_order_detail"})
     */
    protected $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $deletedAt;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     *
     * @var int
     */
    protected $reverseCode;

    /**
     * Constructor.
     *
     * @param string $type Type of Transaction Webpay
     * @param string $code Code of transaction
     */
    public function __construct($type, $code = null)
    {
        $this->webpayType = $type;
        $this->code = $code;
        $this->createdAt = new \DateTime();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code.
     *
     * @param string $code
     *
     * @return self
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code.
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set status.
     *
     * @param string $status
     *
     * @return self
     */
    public function setStatus($status)
    {
        $statusMap = WebpayBuyOrderNormalTrait::getStatusMap();

        if (!in_array($status, array_keys($statusMap))) {
            throw new \Exception('Unknown status');
        }

        if (self::STATUS_REVERSED === $status && self::TRANSACCION_TYPE_ONECLICK !== $this->getWebpayType()) {
            throw new \Exception('Order cannot be reversed');
        }

        $this->status = $status;

        return $this;
    }

    /**
     * Get status.
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set failStatus.
     *
     * @param string $failStatus
     *
     * @return self
     */
    public function setFailStatus($failStatus)
    {
        $this->failStatus = $failStatus;

        return $this;
    }

    /**
     * Get failStatus.
     *
     * @return string
     */
    public function getFailStatus()
    {
        return $this->failStatus;
    }

    /**
     * Set creditCardEndingNumbers.
     *
     * @param string $creditCardEndingNumbers
     *
     * @return self
     */
    public function setCreditCardEndingNumbers($creditCardEndingNumbers)
    {
        $this->creditCardEndingNumbers = $creditCardEndingNumbers;

        return $this;
    }

    /**
     * Get creditCardEndingNumbers.
     *
     * @return string
     */
    public function getCreditCardEndingNumbers()
    {
        return $this->creditCardEndingNumbers;
    }

    /**
     * Set authorizationCode.
     *
     * @param string $authorizationCode
     *
     * @return self
     */
    public function setAuthorizationCode($authorizationCode)
    {
        $this->authorizationCode = $authorizationCode;

        return $this;
    }

    /**
     * Get authorizationCode.
     *
     * @return string
     */
    public function getAuthorizationCode()
    {
        return $this->authorizationCode;
    }

    /**
     * Set paymentTypeCode.
     *
     * @param string $paymentTypeCode
     *
     * @return self
     */
    public function setPaymentTypeCode($paymentTypeCode)
    {
        $this->paymentTypeCode = $paymentTypeCode;

        return $this;
    }

    /**
     * Get paymentTypeCode.
     *
     * @return string
     */
    public function getPaymentTypeCode()
    {
        return $this->paymentTypeCode;
    }

    /**
     * Set responseCode.
     *
     * @param int $responseCode
     *
     * @return self
     */
    public function setResponseCode($responseCode)
    {
        $this->responseCode = $responseCode;

        return $this;
    }

    /**
     * Get responseCode.
     *
     * @return int
     */
    public function getResponseCode()
    {
        return $this->responseCode;
    }

    /**
     * Set sharesAmount.
     *
     * @param string $sharesAmount
     *
     * @return self
     */
    public function setSharesAmount($sharesAmount)
    {
        $this->sharesAmount = $sharesAmount;

        return $this;
    }

    /**
     * Get sharesAmount.
     *
     * @return string
     */
    public function getSharesAmount()
    {
        return $this->sharesAmount;
    }

    /**
     * Set sharesNumber.
     *
     * @param string $sharesNumber
     *
     * @return self
     */
    public function setSharesNumber($sharesNumber)
    {
        $this->sharesNumber = $sharesNumber;

        return $this;
    }

    /**
     * Get sharesNumber.
     *
     * @return string
     */
    public function getSharesNumber()
    {
        return $this->sharesNumber;
    }

    /**
     * Set transactionDateString.
     *
     * @param string $transactionDateString
     *
     * @return self
     */
    public function setTransactionDateString($transactionDateString)
    {
        $this->transactionDateString = $transactionDateString;

        return $this;
    }

    /**
     * Get transactionDateString.
     *
     * @return string
     */
    public function getTransactionDateString()
    {
        return $this->transactionDateString;
    }

    /**
     * Set vci.
     *
     * @param string $vci
     *
     * @return self
     */
    public function setVci($vci)
    {
        $this->vci = $vci;

        return $this;
    }

    /**
     * Get vci.
     *
     * @return string
     */
    public function getVci()
    {
        return $this->vci;
    }

    /**
     * Set userToken.
     *
     * @param string $userToken
     *
     * @return self
     */
    public function setUserToken($userToken)
    {
        $this->userToken = $userToken;

        return $this;
    }

    /**
     * Get userToken.
     *
     * @return string
     */
    public function getUserToken()
    {
        return $this->userToken;
    }

    /**
     * Set transactionToken.
     *
     * @param string $transactionToken
     *
     * @return self
     */
    public function setTransactionToken($transactionToken)
    {
        $this->transactionToken = $transactionToken;

        return $this;
    }

    /**
     * Get transactionToken.
     *
     * @return string
     */
    public function getTransactionToken()
    {
        return $this->transactionToken;
    }

    /**
     * Set username.
     *
     * @param string $username
     *
     * @return self
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get username.
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set userEmail.
     *
     * @param string $userEmail
     *
     * @return self
     */
    public function setUserEmail($userEmail)
    {
        $this->userEmail = $userEmail;

        return $this;
    }

    /**
     * Get userEmail.
     *
     * @return string
     */
    public function getUserEmail()
    {
        return $this->userEmail;
    }

    /**
     * Set webpayType.
     *
     * @param string $webpayType
     *
     * @return self
     */
    public function setWebpayType($webpayType)
    {
        $this->webpayType = $webpayType;

        return $this;
    }

    /**
     * Get webpayType.
     *
     * @return string
     */
    public function getWebpayType()
    {
        return $this->webpayType;
    }

    /**
     * Set amount.
     *
     * @param float $amount
     *
     * @return self
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount.
     *
     * @return float
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set creditCardType.
     *
     * @param string $creditCardType
     *
     * @return self
     */
    public function setCreditCardType($creditCardType)
    {
        $this->creditCardType = $creditCardType;

        return $this;
    }

    /**
     * Get creditCardType.
     *
     * @return string
     */
    public function getCreditCardType()
    {
        return $this->creditCardType;
    }

    /**
     * Set transactionId.
     *
     * @param int $transactionId
     *
     * @return self
     */
    public function setTransactionId($transactionId)
    {
        $this->transactionId = $transactionId;

        return $this;
    }

    /**
     * Get transactionId.
     *
     * @return int
     */
    public function getTransactionId()
    {
        return $this->transactionId;
    }

    /**
     * Set reverseCode.
     *
     * @param int $reverseCode
     *
     * @return self
     */
    public function setReverseCode($reverseCode)
    {
        $this->reverseCode = $reverseCode;

        return $this;
    }

    /**
     * Get reverseCode.
     *
     * @return int
     */
    public function getReverseCode()
    {
        return $this->reverseCode;
    }

    /**
     * Sets deletedAt.
     *
     * @return self
     */
    public function setDeletedAt(\DateTime $deletedAt = null)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Returns deletedAt.
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * Is deleted?
     *
     * @return bool
     */
    public function isDeleted()
    {
        return null !== $this->deletedAt;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime $createdAt
     *
     * @return self
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }
}
