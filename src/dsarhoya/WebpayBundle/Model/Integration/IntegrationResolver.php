<?php

namespace dsarhoya\WebpayBundle\Model\Integration;

use dsarhoya\WebpayBundle\Model\Integration\Api\ApiIntegration;
use dsarhoya\WebpayBundle\Model\Integration\Certificates\CertificatesIntegration;
use dsarhoya\WebpayBundle\Services\WebpayConfigService;

class IntegrationResolver
{
    private $webpayConfigService;
    private $certificatesIntegration;
    private $apiIntegration;

    public function __construct(
        WebpayConfigService $webpayConfigService,
        CertificatesIntegration $certificatesIntegration,
        ApiIntegration $apiIntegration
    ) {
        $this->webpayConfigService = $webpayConfigService;
        $this->certificatesIntegration = $certificatesIntegration;
        $this->apiIntegration = $apiIntegration;
    }

    public function integration(): Integration
    {
        $type = $this->webpayConfigService->getPreferredIntegration();
        
        return 'api' === $type ? $this->apiIntegration : $this->certificatesIntegration;
    }
}
