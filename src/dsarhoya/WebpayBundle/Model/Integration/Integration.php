<?php

namespace dsarhoya\WebpayBundle\Model\Integration;

use dsarhoya\WebpayBundle\Model\Integration\Interfaces\ProcessInterface;
use dsarhoya\WebpayBundle\Model\Integration\Interfaces\TransactionInterface;

abstract class Integration
{
    abstract public function transaction(): TransactionInterface;

    abstract public function process(): ProcessInterface;
}
