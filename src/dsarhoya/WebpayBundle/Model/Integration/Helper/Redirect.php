<?php

namespace dsarhoya\WebpayBundle\Model\Integration\Helper;

class Redirect
{
    public const REDIRECT_TYPE_POST = 'post_redirect';
    public const REDIRECT_TYPE_GET = 'get_redirect';

    public $type;
    public $url;
    public $token;

    public function __construct(string $type, string $url, string $token = null)
    {
        $this->type = $type;
        $this->url = $url;
        $this->token = $token;
    }

    public static function with(string $type, string $url, string $token = null): self
    {
        return new self($type, $url, $token);
    }
}
