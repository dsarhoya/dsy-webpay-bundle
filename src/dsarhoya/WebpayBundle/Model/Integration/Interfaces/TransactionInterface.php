<?php

namespace dsarhoya\WebpayBundle\Model\Integration\Interfaces;

use dsarhoya\WebpayBundle\Model\Integration\Helper\Redirect;

interface TransactionInterface
{
    public function init(array $options): self;

    public function redirect(array $options = null): Redirect;
}
