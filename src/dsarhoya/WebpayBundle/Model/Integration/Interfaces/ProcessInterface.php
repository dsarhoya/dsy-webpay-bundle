<?php

namespace dsarhoya\WebpayBundle\Model\Integration\Interfaces;

use dsarhoya\WebpayBundle\Model\Integration\Helper\Redirect;

interface ProcessInterface
{
    public function process(array $options): ProcessInterface;

    public function commit(array $options): ProcessInterface;

    public function redirect(array $options = null): Redirect;
}
