<?php

namespace dsarhoya\WebpayBundle\Model\Integration\Exception;

use dsarhoya\WebpayBundle\Interfaces\WebpayBuyOrderInterface;

class WebpayException extends \Exception
{
    // private $order;
    private $webpayErrorCode;

    public function __construct(WebpayBuyOrderInterface $order, string $code)
    {
        $this->order = $order;
        $this->webpayErrorCode = $code;
        parent::__construct('Error '.__CLASS__);
    }

    public static function with(WebpayBuyOrderInterface $order, $code): self
    {
        return new self($order, $code);
    }

    public function getWebpayErrorCode()
    {
        return $this->webpayErrorCode;
    }
}
