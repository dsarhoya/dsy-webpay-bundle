<?php

namespace dsarhoya\WebpayBundle\Model\Integration\Exception;

use dsarhoya\WebpayBundle\Interfaces\WebpayBuyOrderInterface;
use dsarhoya\WebpayBundle\Model\Integration\Helper\Redirect;

class AlreadyPayedException extends \Exception
{
    private $redirect;

    public function __construct(WebpayBuyOrderInterface $order, Redirect $redirect)
    {
        $this->order = $order;
        $this->redirect = $redirect;
        parent::__construct('Error '.__CLASS__);
    }

    public static function with(WebpayBuyOrderInterface $order, Redirect $redirect): self
    {
        return new self($order, $redirect);
    }

    /**
     * @return Redirect
     */
    public function getRedirect()
    {
        return $this->redirect;
    }
}
