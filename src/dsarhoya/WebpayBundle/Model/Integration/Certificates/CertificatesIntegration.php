<?php

namespace dsarhoya\WebpayBundle\Model\Integration\Certificates;

use Doctrine\ORM\EntityManagerInterface;
use dsarhoya\WebpayBundle\Model\Integration\Integration;
use dsarhoya\WebpayBundle\Model\Integration\Interfaces\ProcessInterface;
use dsarhoya\WebpayBundle\Model\Integration\Interfaces\TransactionInterface;
use dsarhoya\WebpayBundle\Services\WebpayService;
use Symfony\Component\Routing\RouterInterface;

class CertificatesIntegration extends Integration
{
    private $webpayService;
    private $router;
    private $em;

    public function __construct(
        WebpayService $webpayService,
        RouterInterface $router,
        EntityManagerInterface $em
    ) {
        $this->webpayService = $webpayService;
        $this->router = $router;
        $this->em = $em;
    }

    public function transaction(): TransactionInterface
    {
        return new Transaction($this->webpayService, $this->router);
    }

    public function process(): ProcessInterface
    {
        return new Process($this->webpayService, $this->router, $this->em);
    }
}
