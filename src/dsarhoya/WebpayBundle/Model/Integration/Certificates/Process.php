<?php

namespace dsarhoya\WebpayBundle\Model\Integration\Certificates;

use Doctrine\ORM\EntityManagerInterface;
use dsarhoya\WebpayBundle\Interfaces\WebpayBuyOrderInterface;
use dsarhoya\WebpayBundle\Model\Integration\Exception\AlreadyPayedException;
use dsarhoya\WebpayBundle\Model\Integration\Exception\WebpayException;
use dsarhoya\WebpayBundle\Model\Integration\Helper\Redirect;
use dsarhoya\WebpayBundle\Model\Integration\Interfaces\ProcessInterface;
use dsarhoya\WebpayBundle\Services\WebpayService;
use Freshwork\Transbank\Exceptions\InvalidCertificateException;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\RouterInterface;

class Process implements ProcessInterface
{
    private $webpayService;
    private $router;
    private $em;
    private $plus;

    /**
     * @var \Freshwork\Transbank\WebpayStandard\transactionResultOutput
     */
    private $transactionResult;

    public function __construct(
        WebpayService $webpayService,
        RouterInterface $router,
        EntityManagerInterface $em
    ) {
        $this->webpayService = $webpayService;
        $this->router = $router;
        $this->em = $em;
    }

    public function process(array $options): ProcessInterface
    {
        /** @var WebpayBuyOrderInterface $order */
        $order = $options['order'];

        if (WebpayBuyOrderInterface::STATUS_PAYED === $order->getStatus()) {
            $url = Redirect::with(Redirect::REDIRECT_TYPE_GET, $this->router->generate('webpay_normal_final', [
                'orderId' => $order->getId(),
            ], UrlGeneratorInterface::ABSOLUTE_URL));

            throw AlreadyPayedException::with($order, $url);
        }

        $this->plus = $this->webpayService->getWebpayClient(WebpayBuyOrderInterface::TRANSACCION_TYPE_NORMAL, ['order' => $order]);

        try {
            $this->transactionResult = $this->plus->getTransactionResult();
        } catch (InvalidCertificateException $e) {
            throw WebpayException::with($order, WebpayBuyOrderInterface::FAIL_STATUS_CERTIFICATE_VALIDATION);
        }

        /*
        if (!key_exists('VCI', $response)) {//not a valid answer from transbank
            return $this->failWith($order, WebpayBuyOrderInterface::FAIL_STATUS_NO_VCI);
        }
        */

        $order->setCreditCardEndingNumbers((string) $this->transactionResult->cardDetail->cardNumber);
        $order->setAuthorizationCode((string) $this->transactionResult->detailOutput->authorizationCode);
        $order->setPaymentTypeCode((string) $this->transactionResult->detailOutput->paymentTypeCode);
        $order->setResponseCode((int) $this->transactionResult->detailOutput->responseCode);
        $order->setSharesNumber((string) $this->transactionResult->detailOutput->sharesNumber);
        if ($this->transactionResult->detailOutput->sharesNumber > 0) {
            $order->setSharesAmount((string) $this->transactionResult->detailOutput->sharesAmount);
        }
        $order->setTransactionDateString((string) $this->transactionResult->transactionDate);
        $order->setVci((string) $this->transactionResult->VCI);

        $this->em->flush();

        //not successful. Este caso se da cuando la persona no se logra identificar en su portal bancario.
        if (WebpayBuyOrderInterface::VCI_SUCCESSFULL_AUTHENTICATION !== $order->getVci() || WebpayBuyOrderInterface::RESPONSE_CODE_APPROVED_TRANSACTION !== $order->getResponseCode()) {
            throw WebpayException::with($order, WebpayBuyOrderInterface::FAIL_STATUS_GENERAL);
        }

        //Terminar. El evento se lanza desde el controlador

        return $this;
    }

    public function commit(array $options): ProcessInterface
    {
        /** @var WebpayBuyOrderInterface $order */
        $order = $options['order'];

        try {
            $this->plus->acknowledgeTransaction();
        } catch (\SoapFault $e) {
            throw WebpayException::with($order, WebpayBuyOrderInterface::FAIL_STATUS_TIMEOUT);
        }

        $order->setStatus(WebpayBuyOrderInterface::STATUS_PAYED);

        $this->em->flush();

        return $this;
    }

    public function redirect(array $options = null): Redirect
    {
        return new Redirect(Redirect::REDIRECT_TYPE_POST, $this->transactionResult->urlRedirection);
    }
}
