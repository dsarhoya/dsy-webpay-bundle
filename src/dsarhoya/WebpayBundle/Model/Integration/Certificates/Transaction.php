<?php

namespace dsarhoya\WebpayBundle\Model\Integration\Certificates;

use dsarhoya\WebpayBundle\Interfaces\WebpayBuyOrderInterface;
use dsarhoya\WebpayBundle\Model\Integration\Exception\WebpayException;
use dsarhoya\WebpayBundle\Model\Integration\Helper\Redirect;
use dsarhoya\WebpayBundle\Model\Integration\Interfaces\TransactionInterface;
use dsarhoya\WebpayBundle\Services\WebpayService;
use Freshwork\Transbank\Exceptions\InvalidCertificateException;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\RouterInterface;

class Transaction implements TransactionInterface
{
    private $webpayService;
    private $router;

    /**
     * @var \Freshwork\Transbank\WebpayStandard\wsInitTransactionOutput
     */
    private $initResponse;

    public function __construct(
        WebpayService $webpayService,
        RouterInterface $router
    ) {
        $this->webpayService = $webpayService;
        $this->router = $router;
    }

    public function init(array $options): TransactionInterface
    {
        /** @var WebpayBuyOrderInterface $order */
        $order = $options['order'];

        /** @var \Freshwork\Transbank\WebpayNormal $plus */
        $plus = $this->webpayService->getWebpayClient($options['type'], ['order' => $order]);

        //Estable el monto a pagar y el codicgo de la order.
        $plus->addTransactionDetail($order->getAmount(), $order->getCode());

        $intermediateUrl = $this->router->generate('webpay_normal_return', [
            'orderId' => $order->getId(),
        ], UrlGeneratorInterface::ABSOLUTE_URL);

        $thanksUrl = $this->router->generate('webpay_normal_final', [
            'orderId' => $order->getId(),
        ], UrlGeneratorInterface::ABSOLUTE_URL);

        try {
            $this->initResponse = $plus->initTransaction($intermediateUrl, $thanksUrl);
        } catch (InvalidCertificateException $e) {
            throw WebpayException::with($order, WebpayBuyOrderInterface::FAIL_STATUS_CERTIFICATE_VALIDATION);
        }

        return $this;
    }

    public function redirect(array $options = null): Redirect
    {
        return new Redirect(Redirect::REDIRECT_TYPE_POST, $this->initResponse->url, $this->initResponse->token);
    }
}
