<?php

namespace dsarhoya\WebpayBundle\Model\Integration\Api;

use Doctrine\ORM\EntityManagerInterface;
use dsarhoya\WebpayBundle\Interfaces\WebpayBuyOrderInterface;
use dsarhoya\WebpayBundle\Model\Integration\Exception\AlreadyPayedException;
use dsarhoya\WebpayBundle\Model\Integration\Exception\WebpayException;
use dsarhoya\WebpayBundle\Model\Integration\Helper\Redirect;
use dsarhoya\WebpayBundle\Model\Integration\Interfaces\ProcessInterface;
use dsarhoya\WebpayBundle\Services\WebpayService;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\RouterInterface;
use Transbank\Webpay\WebpayPlus\Responses\TransactionCommitResponse;

class Process implements ProcessInterface
{
    private $webpayService;
    private $router;
    private $em;

    public function __construct(
        WebpayService $webpayService,
        RouterInterface $router,
        EntityManagerInterface $em
    ) {
        $this->webpayService = $webpayService;
        $this->router = $router;
        $this->em = $em;
    }

    public function process(array $options): ProcessInterface
    {
        /** @var WebpayBuyOrderInterface $order */
        $order = $options['order'];

        if (WebpayBuyOrderInterface::STATUS_PAYED === $order->getStatus()) {
            $url = Redirect::with(Redirect::REDIRECT_TYPE_GET, $this->router->generate('webpay_normal_final', [
                'orderId' => $order->getId(),
            ], UrlGeneratorInterface::ABSOLUTE_URL));

            throw AlreadyPayedException::with($order, $url);
        }

        if ($this->userAbortedOnWebpayForm()) {
            throw WebpayException::with($order, WebpayBuyOrderInterface::FAIL_STATUS_DECLINED_BY_USER);
        }

        if ($this->anErrorOcurredOnWebpayForm()) {
            throw WebpayException::with($order, WebpayBuyOrderInterface::FAIL_STATUS_GENERAL);
        }

        if ($this->anErrorOcurredOnWebpayForm()) {
            throw WebpayException::with($order, WebpayBuyOrderInterface::FAIL_STATUS_GENERAL);
        }

        if ($this->theUserWasRedirectedBecauseWasIdleFor10MinutesOnWebapayForm()) {
            throw WebpayException::with($order, WebpayBuyOrderInterface::FAIL_STATUS_TIMEOUT);
        }

        if (!$this->isNormalPaymentFlow()) {
            throw WebpayException::with($order, WebpayBuyOrderInterface::FAIL_STATUS_GENERAL);
        }

        return $this;
    }

    public function commit(array $options): ProcessInterface
    {
        /** @var WebpayBuyOrderInterface $order */
        $order = $options['order'];

        $transaction = $this->webpayService->getTransaction($options['type'], ['order' => $order]);

        $token = $_GET['token_ws'] ?? $_POST['token_ws'] ?? null; // Obtener el token de un flujo normal

        /** @var TransactionCommitResponse $response */
        $response = $transaction->commit($token);

        $order->setCreditCardEndingNumbers((string) $response->getCardNumber());
        $order->setAuthorizationCode((string) $response->getAuthorizationCode());
        $order->setPaymentTypeCode((string) $response->getPaymentTypeCode());
        $order->setResponseCode((int) $response->getResponseCode());
        $order->setSharesNumber((string) $response->getInstallmentsNumber());
        if ($response->getInstallmentsNumber() > 0) {
            $order->setSharesAmount((string) $response->getInstallmentsAmount());
        }
        $order->setTransactionDateString((string) $response->getTransactionDate());
        $order->setVci((string) $response->getVci());

        $this->em->flush();

        if (!$response->isApproved()) {
            throw WebpayException::with($order, WebpayBuyOrderInterface::FAIL_STATUS_GENERAL);
        }

        $order->setStatus(WebpayBuyOrderInterface::STATUS_PAYED);

        $this->em->flush();

        return $this;
    }

    public function redirect(array $options = null): Redirect
    {
        /** @var WebpayBuyOrderInterface $order */
        $order = $options['order'];

        $thanksUrl = $this->router->generate('webpay_normal_final', [
            'orderId' => $order->getId(),
        ], UrlGeneratorInterface::ABSOLUTE_URL);

        return new Redirect(Redirect::REDIRECT_TYPE_GET, $thanksUrl);
    }

    private function userAbortedOnWebpayForm()
    {
        $tokenWs = $_GET['token_ws'] ?? $_POST['token_ws'] ?? null;
        $tbkToken = $_GET['TBK_TOKEN'] ?? $_POST['TBK_TOKEN'] ?? null;
        $ordenCompra = $_GET['TBK_ORDEN_COMPRA'] ?? $_POST['TBK_ORDEN_COMPRA'] ?? null;
        $idSesion = $_GET['TBK_ID_SESION'] ?? $_POST['TBK_ID_SESION'] ?? null;

        // Si viene TBK_TOKEN, TBK_ORDEN_COMPRA y TBK_ID_SESION es porque el usuario abortó el pago
        return $tbkToken && $ordenCompra && $idSesion && !$tokenWs;
    }

    private function anErrorOcurredOnWebpayForm()
    {
        $tokenWs = $_GET['token_ws'] ?? $_POST['token_ws'] ?? null;
        $tbkToken = $_GET['TBK_TOKEN'] ?? $_POST['TBK_TOKEN'] ?? null;
        $ordenCompra = $_GET['TBK_ORDEN_COMPRA'] ?? $_POST['TBK_ORDEN_COMPRA'] ?? null;
        $idSesion = $_GET['TBK_ID_SESION'] ?? $_POST['TBK_ID_SESION'] ?? null;

        // Si viene token_ws, TBK_TOKEN, TBK_ORDEN_COMPRA y TBK_ID_SESION es porque ocurrió un error en el formulario de pago
        return $tokenWs && $ordenCompra && $idSesion && $tbkToken;
    }

    private function theUserWasRedirectedBecauseWasIdleFor10MinutesOnWebapayForm()
    {
        $tokenWs = $_GET['token_ws'] ?? $_POST['token_ws'] ?? null;
        $tbkToken = $_GET['TBK_TOKEN'] ?? $_POST['TBK_TOKEN'] ?? null;
        $ordenCompra = $_GET['TBK_ORDEN_COMPRA'] ?? $_POST['TBK_ORDEN_COMPRA'] ?? null;
        $idSesion = $_GET['TBK_ID_SESION'] ?? $_POST['TBK_ID_SESION'] ?? null;

        // Si viene solo TBK_ORDEN_COMPRA y TBK_ID_SESION es porque el usuario estuvo 10 minutos sin hacer nada en el
        // formulario de pago y se canceló la transacción automáticamente (por timeout)
        return $ordenCompra && $idSesion && !$tokenWs && !$tbkToken;
    }

    private function isNormalPaymentFlow()
    {
        $tokenWs = $_GET['token_ws'] ?? $_POST['token_ws'] ?? null;
        $tbkToken = $_GET['TBK_TOKEN'] ?? $_POST['TBK_TOKEN'] ?? null;
        $ordenCompra = $_GET['TBK_ORDEN_COMPRA'] ?? $_POST['TBK_ORDEN_COMPRA'] ?? null;
        $idSesion = $_GET['TBK_ID_SESION'] ?? $_POST['TBK_ID_SESION'] ?? null;

        // Si viene solo token_ws es porque es un flujo de pago normal
        return $tokenWs && !$ordenCompra && !$idSesion && !$tbkToken;
    }
}
