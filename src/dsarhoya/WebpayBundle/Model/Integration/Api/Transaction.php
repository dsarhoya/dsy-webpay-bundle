<?php

namespace dsarhoya\WebpayBundle\Model\Integration\Api;

use Doctrine\ORM\EntityManagerInterface;
use dsarhoya\WebpayBundle\Interfaces\WebpayBuyOrderInterface;
use dsarhoya\WebpayBundle\Model\Integration\Helper\Redirect;
use dsarhoya\WebpayBundle\Model\Integration\Interfaces\TransactionInterface;
use dsarhoya\WebpayBundle\Services\WebpayService;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\RouterInterface;

class Transaction implements TransactionInterface
{
    private $webpayService;
    private $router;
    private $em;

    private $createResponse;

    public function __construct(
        WebpayService $webpayService,
        RouterInterface $router,
        EntityManagerInterface $em
    ) {
        $this->webpayService = $webpayService;
        $this->router = $router;
        $this->em = $em;
    }

    public function init(array $options): TransactionInterface
    {
        /** @var WebpayBuyOrderInterface $order */
        $order = $options['order'];

        $intermediateUrl = $this->router->generate('webpay_normal_return', [
            'orderId' => $order->getId(),
        ], UrlGeneratorInterface::ABSOLUTE_URL);

        $transaction = $this->webpayService->getTransaction($options['type'], ['order' => $order]);

        $this->createResponse = $transaction->create($order->getCode(), uniqid(), $order->getAmount(), $intermediateUrl);

        $order->setTransactionToken($this->createResponse->getToken());

        $this->em->flush();

        return $this;
    }

    public function redirect(array $options = null): Redirect
    {
        $redirectUrl = $this->createResponse->getUrl().'?token_ws='.$this->createResponse->getToken();

        return new Redirect(Redirect::REDIRECT_TYPE_GET, $redirectUrl);
    }
}
