<?php

namespace dsarhoya\WebpayBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/configuration.html}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('dsy_webpay');

        $rootNode
            ->children()
                ->arrayNode('classes')
                    ->children()
                        ->arrayNode('webpay_buy_order')
                            ->children()
                                ->scalarNode('class')->isRequired()->end()
                            ->end()
                        ->end()
                        ->arrayNode('one_click_inscription')
                            ->children()
                                ->scalarNode('class')->isRequired()->end()
                            ->end()
                        ->end()
                    ->end()
                ->end()
                ->arrayNode('certs')
                    ->children()
                        ->arrayNode('prod')
                            ->children()
                                ->scalarNode('key_name')->isRequired()->end()
                                ->scalarNode('crt_name')->isRequired()->end()
                                ->scalarNode('pem_name')->isRequired()->end()
                            ->end()
                        ->end()
                        ->scalarNode('force_env')->end()
                    ->end()
                ->end()
                ->arrayNode('api')
                    ->children()
                        ->scalarNode('commerce_code')->isRequired()->end()
                        ->scalarNode('api_key')->isRequired()->end()
                    ->end()
                ->end()
                ->scalarNode('env')->end()
            ->end();

        return $treeBuilder;
    }
}
