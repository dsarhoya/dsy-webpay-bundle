<?php

namespace dsarhoya\WebpayBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;

/**
 * This is the class that loads and manages your bundle configuration.
 *
 * @link http://symfony.com/doc/current/cookbook/bundles/extension.html
 */
class DSYWebpayExtension extends Extension {

    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container) {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $container->setParameter('dsy_webpay.classes', $config['classes']);
        $container->setParameter('dsy_webpay.certs', isset($config['certs']) ? $config['certs'] : null);
        $container->setParameter('dsy_webpay.api', isset($config['api']) ? $config['api'] : null);
        $container->setParameter('dsy_webpay.env', isset($config['env']) ? $config['env'] : null);

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config'));
        $loader->load('services.yml');
    }

}
