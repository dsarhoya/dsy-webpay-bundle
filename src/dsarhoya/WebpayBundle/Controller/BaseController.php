<?php

namespace dsarhoya\WebpayBundle\Controller;

use AppBundle\Repository\WebpayBuyOrderRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Event\GetResponseForControllerResultEvent;
use Symfony\Component\HttpKernel\HttpKernelInterface;

/**
 * Description of BaseController.
 *
 * @author snake77se <yosip.curiel@dsarhoya.cl>
 */
class BaseController extends Controller
{
    protected $classes;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
        $this->classes = $this->container->getParameter('dsy_webpay.classes');
    }

    /**
     * @param string $eventName
     */
    protected function dispatchGetResposeForControllerEvent($eventName, array $arguments = [])
    {
        $event = new GetResponseForControllerResultEvent(
                $this->container->get('kernel'),
                $this->get('request_stack')->getCurrentRequest(),
                HttpKernelInterface::MASTER_REQUEST, $arguments['entity']
        );
        $this->getServiceEventDispacher()->dispatch($eventName, $event);

        return $event;
    }

    /**
     * @return \Symfony\Component\HttpKernel\Debug\TraceableEventDispatcher
     */
    protected function getServiceEventDispacher()
    {
        return $this->get('event_dispatcher');
    }

    /**
     * @return \dsarhoya\WebpayBundle\Services\WebpayService
     */
    protected function getServiceWebpay()
    {
        return $this->get('dsy.webpay.service');
    }

    /**
     * @return \dsarhoya\WebpayBundle\Model\Integration\IntegrationResolver
     */
    protected function getIntegreationResolver()
    {
        return $this->get('dsy.webpay.integration_resolver');
    }

    /**
     * @param string $class
     *
     * @return \Doctrine\ORM\Repository\RepositoryFactory
     */
    protected function getRepo($class)
    {
        return $this->getDoctrine()->getRepository($class);
    }

    /**
     * @param string $class
     *
     * @return WebpayBuyOrderRepository
     */
    protected function getRepoWebpayBuyOrder()
    {
        return $this->getRepo($this->classes['webpay_buy_order']['class']);
    }

    protected function getRepoOneClickInscription()
    {
        return $this->getRepo($this->classes['one_click_inscription']['class']);
    }
}
