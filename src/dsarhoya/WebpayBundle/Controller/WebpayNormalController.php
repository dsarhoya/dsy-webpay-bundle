<?php

namespace dsarhoya\WebpayBundle\Controller;

use dsarhoya\WebpayBundle\Event\DSYWebpayBundleEvents;
use dsarhoya\WebpayBundle\Interfaces\WebpayBuyOrderInterface;
// use Freshwork\Transbank\Exceptions\InvalidCertificateException;
use dsarhoya\WebpayBundle\Model\Integration\Exception\AlreadyPayedException;
use dsarhoya\WebpayBundle\Model\Integration\Exception\WebpayException;
use dsarhoya\WebpayBundle\Model\Integration\Helper\Redirect;
use Freshwork\Transbank\RedirectorHelper;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
// use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Transbank\Webpay\WebpayPlus;

/**
 * Description of WebpayNormalController.
 *
 * @Route("/normal")
 *
 * @author snake77se <yosip.curiel@dsarhoya.cl>
 */
class WebpayNormalController extends BaseController
{
    /**
     * @Route("/{orderId}/start", name="webpay_normal_start")
     * @Method({"GET"})
     *
     * @return Response
     */
    public function startAction(Request $request, $orderId)
    {
        if (null === $orderId || null === ($order = $this->getRepoWebpayBuyOrder()->find($orderId))) {
            throw new \Exception("Webpay buy order '{$orderId}' not found");
        }

        $transaction = $this->getIntegreationResolver()->integration()->transaction();

        try {
            $transaction->init([
                'type' => WebpayBuyOrderInterface::TRANSACCION_TYPE_NORMAL,
                'order' => $order,
            ]);
        } catch (WebpayException $e) {
            return $this->failWith($order, $e->getWebpayErrorCode());
        }

        $redirect = $transaction->redirect([
            'order' => $order,
        ]);

        if (Redirect::REDIRECT_TYPE_POST === $redirect->type) {
            $redirectAutoSubmit = RedirectorHelper::redirectHTML($redirect->url, $redirect->token);
            
            return $this->render('@DSYWebpay/Normal/start.html.twig', [
                'redirectAutoSubmit' => $redirectAutoSubmit,
            ]);
        }

        if (Redirect::REDIRECT_TYPE_GET === $redirect->type) {
            return $this->redirect($redirect->url);
        }

        throw new \Exception("Redirect tipo {$redirect->type} no implementado");
    }

    /**
     * @Route("/{orderId}/return", name="webpay_normal_return")
     * @Method({"GET","POST"})
     *
     * @return Response
     */
    public function returnAction(Request $request, $orderId)
    {
        if (null === $orderId || null === ($order = $this->getRepoWebpayBuyOrder()->find($orderId))) {
            throw new \Exception("Webpay buy order '{$orderId}' not found");
        }

        $process = $this->getIntegreationResolver()->integration()->process();

        try {
            $process->process([
                'type' => WebpayBuyOrderInterface::TRANSACCION_TYPE_NORMAL,
                'order' => $order,
            ]);
        } catch (WebpayException $e) {
            return $this->failWith($order, $e->getWebpayErrorCode());
        } catch (AlreadyPayedException $e){
            
            $redirect = $e->getRedirect();

            if (Redirect::REDIRECT_TYPE_POST === $redirect->type) {
                $redirectAutoSubmit = RedirectorHelper::redirectBackNormal($redirect->url);

                return $this->render('DSYWebpayBundle:Normal:return.html.twig', [
                    'redirectAutoSubmit' => $redirectAutoSubmit,
                ]);
            }

            if (Redirect::REDIRECT_TYPE_GET === $redirect->type) {
                return $this->redirect($redirect->url);
            }
        }


        //Esto viene de la rama v4, validar que no se ocupa.
        // //not successful. Este caso se da cuando la persona no se logra identificar en su portal bancario.
        // if (WebpayBuyOrderInterface::VCI_SUCCESSFULL_AUTHENTICATION !== $order->getVci() || WebpayBuyOrderInterface::RESPONSE_CODE_APPROVED_TRANSACTION !== $order->getResponseCode()) {
        //     return $this->failWith($order, WebpayBuyOrderInterface::FAIL_STATUS_GENERAL);
        // }
        $event = $this->dispatchGetResposeForControllerEvent(DSYWebpayBundleEvents::NORMAL_PAYMENT_PRE_ACKNOWLEDGE_TRANSACTION, ['entity' => $order]);

        if ($event->getResponse()) {
            return $event->getResponse();
        }

        // Ahora el método commit del process
        try {
            $process->commit([
                'type' => WebpayBuyOrderInterface::TRANSACCION_TYPE_NORMAL,
                'order' => $order,
            ]);
        } catch (WebpayException $e) {
            return $this->failWith($order, $e->getWebpayErrorCode());
        }

        $event = $this->dispatchGetResposeForControllerEvent(DSYWebpayBundleEvents::NORMAL_PAYMENT_POST_ACKNOWLEDGE_TRANSACTION, ['entity' => $order]);

        if ($event->getResponse()) {
            return $event->getResponse();
        }

        $redirect = $process->redirect([
            'order' => $order,
        ]);

        if (Redirect::REDIRECT_TYPE_POST === $redirect->type) {
            $redirectAutoSubmit = RedirectorHelper::redirectBackNormal($redirect->url);

            return $this->render('@DSYWebpay/Normal/return.html.twig', [
                'redirectAutoSubmit' => $redirectAutoSubmit,
            ]);
        }

        if (Redirect::REDIRECT_TYPE_GET === $redirect->type) {
            return $this->redirect($redirect->url);
        }

        throw new \Exception("Redirect tipo {$redirect->type} no implementado");
    }

    /**
     * @Route("/{orderId}/final", name="webpay_normal_final")
     * @Method({"GET","POST"})
     *
     * @return Response
     */
    public function finalAction(Request $request, $orderId)
    {
        if (null === $orderId || null === ($order = $this->getRepoWebpayBuyOrder()->find($orderId))) {
            throw new \Exception("Webpay buy order '{$orderId}' not found");
        }

        if (WebpayBuyOrderInterface::STATUS_PAYED !== $order->getStatus()) {
            return $this->failWith($order, WebpayBuyOrderInterface::FAIL_STATUS_DECLINED_BY_USER);
        }

        $event = $this->dispatchGetResposeForControllerEvent(DSYWebpayBundleEvents::NORMAL_PAYMENT_SUCCESS, ['entity' => $order]);

        if ($event->getResponse()) {
            return $event->getResponse();
        }

        return $this->render('@DSYWebpay/Normal/finish.html.twig', [
            'order' => $order,
        ]);
    }

    /**
     * @Route("/{orderId}/fail", name="webpay_normal_fail")
     * @Method({"GET","POST"})
     *
     * @return Response
     */
    public function failAction(Request $request, $orderId)
    {
        if (null === $orderId || null === ($order = $this->getRepoWebpayBuyOrder()->find($orderId))) {
            throw new \Exception("Webpay buy order '{$orderId}' not found");
        }

        $event = $this->dispatchGetResposeForControllerEvent(DSYWebpayBundleEvents::NORMAL_PAYMENT_FAILURE, ['entity' => $order]);

        if ($event->getResponse()) {
            return $event->getResponse();
        }

        return $this->render('@DSYWebpay/Normal/fail.html.twig', [
            'order' => $order,
        ]);
    }

    /**
     * Process Fail.
     *
     * @param string $failStatus
     *
     * @return Response
     */
    private function failWith(WebpayBuyOrderInterface $order, $failStatus)
    {
        $order->setFailStatus($failStatus);
        $order->setStatus(WebpayBuyOrderInterface::STATUS_FAILED);
        $this->getDoctrine()->getManager()->flush($order);

        return $this->redirectToRoute('webpay_normal_fail', [
            'orderId' => $order->getId(),
        ]);
    }
}
