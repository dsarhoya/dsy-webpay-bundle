<?php

namespace dsarhoya\WebpayBundle\Controller;

use dsarhoya\WebpayBundle\Interfaces\WebpayBuyOrderInterface;
use dsarhoya\WebpayBundle\Interfaces\OneClickIncriptionInterface;
use dsarhoya\WebpayBundle\Interfaces\WebpayOneclickBuyOrderInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Freshwork\Transbank\RedirectorHelper;
use dsarhoya\WebpayBundle\Event\DSYWebpayBundleEvents;

/**
 * Description of OneClickController.
 *
 * @Route("/oneclick")
 *
 * @author snake77se <yosip.curiel@dsarhoya.cl>
 */
class OneClickController extends BaseController
{
    /**
     * Init Inscription OneClick.
     *
     * @Route("/{oneClickInscriptionEntityId}/init-inscription", name="webpay_oneclick_inscription")
     * @Method({"GET"})
     *
     * @param Request $request
     * @param int     $orderId
     *
     * @return Response
     */
    public function initInscriptiontAction($oneClickInscriptionEntityId)
    {
        /* @var $oneclickInscription \dsarhoya\WebpayBundle\Interfaces\OneClickIncriptionInterface */
        if (null === $oneClickInscriptionEntityId || null === ($oneclickInscription = $this->getRepoOneClickInscription()->find($oneClickInscriptionEntityId))) {
            throw new \Exception("OneClickInscriptionEntity '{$oneClickInscriptionEntityId}' not found");
        }

        $url = $this->generateUrl('webpay_oneclick_finish_inscription', ['oneClickInscriptionEntityId' => $oneclickInscription->getId()], UrlGeneratorInterface::ABSOLUTE_URL);

        /* @var $oneClick \Freshwork\Transbank\WebpayOneClick */
        $oneClick = $this->getServiceWebpay()->getWebpayClient(WebpayBuyOrderInterface::TRANSACCION_TYPE_ONECLICK, ['inscription' => $oneclickInscription]);

        /* @var $response \Freshwork\Transbank\WebpayOneClick\oneClickInscriptionOutput */
        $response = $oneClick->initInscription($oneclickInscription->getUsername(), $oneclickInscription->getEmail(), $url);

        return $this->render('DSYWebpayBundle:OneClick:initInscription.html.twig', [
            'redirectAutoSubmit' => RedirectorHelper::redirectHTML($response->urlWebpay, $response->token),
        ]);
    }

    /**
     * FinishInscription OneClick.
     *
     * @param OneClickIncriptionInterface $user
     * @Route("/{oneClickInscriptionEntityId}/finish-inscription", name="webpay_oneclick_finish_inscription")
     * @Method({"POST"})
     *
     * @return Response
     */
    public function finishInscriptionAction(Request $request, $oneClickInscriptionEntityId)
    {
        /* @var $oneclickInscription \dsarhoya\WebpayBundle\Interfaces\OneClickIncriptionInterface */
        if (null === $oneClickInscriptionEntityId || null === ($oneClickInscription = $this->getRepoOneClickInscription()->find($oneClickInscriptionEntityId))) {
            throw new \Exception("OneClickInscriptionEntity '{$oneClickInscriptionEntityId}' not found");
        }

        /* @var $oneClick \Freshwork\Transbank\WebpayOneClick */
        $oneClick = $this->getServiceWebpay()->getWebpayClient(WebpayBuyOrderInterface::TRANSACCION_TYPE_ONECLICK, ['inscription' => $oneClickInscription]);

        /* @var $response \Freshwork\Transbank\WebpayOneClick\oneClickFinishInscriptionOutput */
        $response = $oneClick->finishInscription($request->request->get('TBK_TOKEN'));

        $oneClickInscription->setOneClickResponseCode((int) $response->responseCode);
        $oneClickInscription->setOneClickAuthCode($response->authCode);
        $oneClickInscription->setOneClickCreditCardType($response->creditCardType);
        $oneClickInscription->setOneClickLast4CardDigits($response->last4CardDigits);
        $oneClickInscription->setOneClickToken($response->tbkUser);

        $this->getDoctrine()->getManager()->flush($oneClickInscription);

        $event = $this->dispatchGetResposeForControllerEvent(DSYWebpayBundleEvents::POST_ONE_CLICK_INSCRIPTION, array('entity' => $oneClickInscription));

        if ($event->hasResponse()) {
            return $event->getResponse();
        }

        return $this->render('DSYWebpayBundle:OneClick:finishInscription.html.twig', [
            'data' => $response,
            'oneClickInscription' => $oneClickInscription,
        ]);
    }

    /**
     * Authorize transaction.
     *
     * @Route("/authorize", name="webpay_oneclick_authorize")
     * @Method({"GET"})
     *
     * @param WebpayBuyOrderInterface $order
     *
     * @return Response
     */
    public function authorizeAction(WebpayOneclickBuyOrderInterface $order)
    {
        throw new \Exception('Este método no se tiene que llamar vía wbe, tiene que ser vía servicio.');
        /* @var $oneClick \Freshwork\Transbank\WebpayOneClick */
        // $oneClick = $this->getServiceWebpay()->getWebpayClient($order->getWebpayType(), ['order' => $order]);

        // $this->getServiceWebpay()->authorizeOneClick($order);

        // $event = $this->dispatch(DSYWebpayBundleEvents::POST_ONE_CLICK_AUTHORIZATION, array('entity' => $user));

        // if ($event->hasResponse()) {
        //     return $event->getResponse();
        // }

        // return $this->render('DSYWebpayBundle:Default:authorize.html.twig', [
        //     'order' => $order,
        // ]);
    }

    /**
     * codeReverseOneClick.
     *
     * @Route("/code-reverse", name="webpay_oneclick_code_reverse")
     * @Method({"GET"})
     *
     * @param WebpayBuyOrderInterface $order
     */
    public function codeReverseAction(WebpayOneclickBuyOrderInterface $order)
    {
        throw new \Exception('method not implemented.');
//        $oneClick = $this->getServiceWebpay()->getWebpayClient($order);
//
//        /* @var $reponse \Freshwork\Transbank\WebpayOneClick\oneClickReverseOutput */
//        $response = $oneClick->codeReverseOneClick($order->getCode());
//        //@TODO Terminar reverso
//        return $this->render('', []);
    }

    /**
     * Remove User OneClick.
     *
     * @Route("/remove-user", name="webpay_oneclick_remove_user")
     * @Method({"GET"})
     *
     * @param OneClickIncriptionInterface $user
     *
     * @throws \Exception
     */
    public function removeUserAction(OneClickIncriptionInterface $user)
    {
        throw new \Exception('method not implemented.');
//        $oneClick = $this->getServiceWebpay()->getWebpayClient($order);
//
//        $response = $oneClick->removeUser($order->getUserToken(), $order->getUserName());
//        //@TODO Terminar remove User
//        return $this->render('', []);
    }
}
