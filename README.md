dsy-webpay-bundle
================

Importante, dado que este es un proyecto de desarrollo, el archivo composer.json se ocupa para definición del paquete, no para la importación de dependencias.
Para eso está el archivo composer-dev.json, el cual se tiene que usar de la siguiente manera:
```
COMPOSER=composer-dev.json composer install
```

Pasos para levantar el proyecto:
1. crear un archivo docker-compose-local.yml. Ejemplo:
```
version: '3'
services:
  app:
    ports:
      - 8007:8000 <- esto es importante porque el proyecto va a correr en el puerto 8007
    
    volumes:
      - /home/mati/.ssh:/root/.ssh

    networks:
     - dsy

networks:
  dsy:
    external: true
```

2. Entrar al contenedor e instalar los vendors:

```
COMPOSER=composer-dev.json composer install
```

3. Levantar el servidor de php

```
bin/console server:run 0.0.0.0
```

4. Entrar en el browser al puerto 8007.

Tarjetas de prueba: https://docs.pagofacil.cl/es/articles/1607312-tarjetas-de-prueba-para-transacciones-en-ambiente-de-desarrollo-con-webpay-plus


Pasos en código de una compra
* El proceso parte acá: DefaultController::
* Se crea y guarda un WebpayBuyOrder que tiene toda la información de la compra (code, amount, etc)
* Se redirije al usuario a la ruta webpay_normal_start (dsarhoya\WebpayBundle\Controller\WebpayNormalController)
* El primer paso que hace el bundle es el "start"
* $this->getIntegreationResolver()->integration()->transaction() -> encuentra la estrategia que se quiere ocupar (servicios web soap o api). En el caso de Symfony, se ocupa la api si viene una configuración asociada a la api, si no, se ocupan los certificados (servicios web)
* * En este caso, si en el config.yml está la configuración dsy_webpay.api (commerce_code y api_key)
* se hace la primera llamada a transbank: $transaction->init()
* Se hace una llamada interna a $transaction->redirect para saber a dónde y cómo llevar al usuario.
* Acá inicia el proceso de pago, hasta validar en el banco.
* Después de que el cliente terminó de ingresar toda su info, se manda al cliente de vuelta al sitio para continuar en el método webpay_normal_return
* En este controlador se hace la segunda llamada a transbank: $process->process()
* Si toda la información valida bien, se ejecuta la tercera llamada: $process->commit()
* Se hace una llamada interna a $process->redirect para saber a dónde y cómo llevar al usuario. Acá llevas al usuario a la ventana de éxito de webpay. Acá el cliente hace click en volver al sitio a la ruta webpay_normal_final.
* La ruta webpay_normal_final es local y sólo revisa el estado de la orden en base de datos.

Otros:
* CertificationBag -> una clase donde se guardan en memoria los certificados con que comunicarse con transbank
